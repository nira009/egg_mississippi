__author__ = 'npas004'

import signal_egg as signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import pdb as pdb
import wx as wx
import datetime as datetime
from pylab import *
import findIndices as findIndices
import math as math
import matplotlib.gridspec as gridspec
from scipy import signal as scipySig
from scipy.signal import gaussian
from scipy.ndimage import filters

from scipy.interpolate import interp1d

# Computing the rolling overlaps of the signals
def rolling_window_overlap(a, window, data_Overlap):
	'''
	This method seperates the signals into stacks or strides of the signal
	for computation of rolling stats. By default this method is called from
	the method 'amplitude_frequency_estimation' in this class.
	'''

	data_noOverlap = window - data_Overlap
	indices = np.arange(start=0, stop=a.size - window + 1, step=data_noOverlap)
	shape = indices.shape[-1], window
	strides = (a.strides[-1] * data_noOverlap,) + a.strides
	return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)


def find_nearest(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return array[idx]


def find_nearest_indx(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return idx


def calcFrqPercentage(fftFrq, fft, indx_frq01, indx_frq10):
	'''
	Estimate signal integrity measure. Here the % power of the fft signals are
	given in the range of specified intervals
	'''
	Meas = [None] * len(fft)

	for i in np.arange(0, len(fft)):
		#		Meas[i] = (np.max(fft[i][indx_frq01:indx_frq10]) / np.max(fft[i])) * 100
		A = np.sum(np.abs(fft[i][indx_frq01:indx_frq10]))
		B = np.sum(np.abs(fft[i]))
		Meas[i] = ((A) / (B)) * 100
	return Meas


#Helper function to compute the next power of two
def nextpow2(n):
	m_f = np.log2(n)
	m_i = np.ceil(m_f)
	return (2 ** m_i) * 20


def fftECGsigProc(Sig1, windowSize, overlapWindow):
	print "Frequency estimation"
	# windowSize=	5*30
	# overlapWindow = windowSize-(2*30)
	sig_rollin = rolling_window_overlap(Sig1.sig, window=windowSize, data_Overlap=overlapWindow)
	t_rollin = rolling_window_overlap(Sig1.t, windowSize, overlapWindow)
	t_rollin = np.median(t_rollin, axis=1)

	sig_rollin = scipySig.detrend(sig_rollin, axis=1, type='constant')  #Detrend signal

	# 1. Window signals
	print "window signal"
	hannWind = scipySig.hann(sig_rollin.shape[1], sym=True)
	hannWind = np.tile(hannWind, [sig_rollin.shape[0], 1])
	sig_rollin = sig_rollin * hannWind

	#2. Zero padding for the fft signal
	print "zero pad signal"
	L = nextpow2(sig_rollin.shape[1])
	fourier = fft(sig_rollin, int(L))
	fourier = np.abs(fourier[0:len(fourier) ** 2, 0:L / 2])
	freqs = np.linspace(0, 15, L / 2)
	# pdb.set_trace()
	maxFrqs = freqs[fourier.argmax(axis=1)]
	# print "Frequency (bpm) for entire time - " + str(maxFrqs * 60)  #Convert to cpm

	return maxFrqs, t_rollin


def ecgSignalAnalysisReporting(patient_info, Data, output_file, dataNo, summaryFlg,versionNo):

	msg = "ECG signal {} - Please wait while we process and generate the report...".format(dataNo+1)
	busyDlg = wx.BusyInfo(msg)

	output_file = output_file[:-4] + unicode('_signal_' + str(dataNo + 1) + '.pdf')

	#Initialise test object
	Sig1 = signal_egg.Signal(sig=Data[1][:, dataNo], fs=30.0)

	print "Signal Filtering"
	RawSig = Sig1.sig
	# pdb.set_trace()

	#High frequency removal
	print "Filtering - ECG"
	# # lp_filt = signal_egg.lowpass_filter(filtOrder=5, cutoff=5) # old code version 20160513 - ecg estimates were unreliable
	# # Sig1 = lp_filt(Sig1)
	# hp_filt = signal_egg.highpass_filter(filtOrder=2, cutoff=1)
	# Sig1 = hp_filt(Sig1)
	# lp_filt = signal_egg.lowpass_filter(filtOrder=2, cutoff=5)
	# Sig1 = lp_filt(Sig1)
	# #Smoothing of HF filtered signal
	# b = gaussian(30 * 0.5, 0.1 * 30)
	# ga = filters.convolve1d(Sig1.sig, b / b.sum())
	# Sig1.sig = ga

	baselineLSS = signal_egg.baseline_als(Sig1.sig, lam=1200, p=0.1, niter=10)
	Sig1.sig = Sig1.sig - baselineLSS
	Sig1.sig = Sig1.sig - np.median(Sig1.sig)
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.show()

	lp_filt = signal_egg.lowpass_ButterFilter(filtOrder=5, cutoff=2.5) # was 2.5
	Sig1 = lp_filt(Sig1)
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.show()





	# plt.plot(Sig1.t,Sig1.sig) #testing plotting
	# plt.show()
	# pdb.set_trace()

	print "Frequency estimation"
	# Sig1.sig = np.gradient(Sig1.sig)
	# pdb.set_trace()
	windowSize = 1800 #np.round(60 * 30)
	overlapWindow = windowSize - 300 #windowSize - (20 * 30)
	[maxFrqs, t_rollin] = fftECGsigProc(Sig1, windowSize, overlapWindow)

	# plt.figure(), plt.plot(t_rollin,maxFrqs*60,'*-'),plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.show()
	print "seeting to cpm"
	maxFrqs=maxFrqs*60
	print "seeting limits"
	maxFrqs[maxFrqs < 30] = 0	#Limits of frequency for ECG
	maxFrqs[maxFrqs > 150] = 0	#Limits of frequency for ECG
	print "seeting to nan"
	maxFrqs[maxFrqs==0]=np.nan
	print "interp time"
	# maxFrqs[np.where(maxFrqs>115) or np.where(maxFrqs<55)]=np.nan
	not_nan = np.logical_not(np.isnan(maxFrqs))
	indices = np.arange(len(maxFrqs))
	# interp = interp1d(indices[not_nan], maxFrqs[not_nan])
	# maxFrqs1=interp(indices)
	print "interp "
	maxFrqs2=np.interp(indices, indices[not_nan], maxFrqs[not_nan])
	# plt.figure(), plt.plot(t_rollin,maxFrqs1,'*-'), plt.show()
	# plt.figure(), plt.plot(t_rollin,maxFrqs2,'*-'), plt.show()
	maxFrqs3=scipySig.savgol_filter(maxFrqs2,21,2)
	# plt.figure(), plt.plot(t_rollin,maxFrqs3,'*-'), plt.show()

	# pdb.set_trace()

	#Checking if the estimate is valid
	validPoint = [1]*len(t_rollin)
	validPoint = np.asarray(validPoint)

	# validPoint[maxFrqs < 0.66] = 0	#Limits of frequency for ECG
	# validPoint[maxFrqs > 2] = 0	#Limits of frequency for ECG

	maxFrqs=maxFrqs3
	validPoint[maxFrqs < 30] = 0	#Limits of frequency for ECG # wasx 50
	validPoint[maxFrqs > 160] = 0	#Limits of frequency for ECG # was 115
	maxFrqs=maxFrqs/60

	# # Test plotting
	# f, axarr = plt.subplots(2, sharex=True)
	# axarr[0].plot(Sig1.t, RawSig, Sig1.t, Sig1.sig)
	# axarr[0].set_title('Sharing X axis')
	# axarr[1].plot(t_rollin, maxFrqs, '*-')
	# # axarr[1].scatter(t,maxfr)
	# plt.show()

	#Removing potential errors

	# Compute metrics of interest

	# PLOTTING TO SAVE TO PDF FILE

	# -- 1.0 - get time segments of interets
	#Ask user for time limits
	time_lim = (np.min(Sig1.t) / 60, np.max(Sig1.t) / 60)
	print time_lim

	start_time = np.min(Sig1.t) / 60
	start_time_vec = (find_nearest_indx(Sig1.t, start_time * Sig1.fs))
	end_time = np.max(Sig1.t) / 60
	end_time_vec = (find_nearest_indx(Sig1.t, end_time * Sig1.fs))

	plotLabel = []
	timeSegments = ceil((end_time - start_time) / 5)  # 5 min time segments
	endTimePlot = 0
	startTimePLot = 0

	#First page with average results
	date_now = datetime.datetime.now()
	DateAnaly = 'Date Analysed: '+ date_now.strftime("%A %d. %B %Y ") +date_now.strftime("%H:%M")
#	TimeAnaly = 'Time Analysed: '+ date_now.strftime("%H:%M")
	title = 'ver-' + str(versionNo) + '--- Date Analysed: '+ date_now.strftime("%A %d. %B %Y") +date_now.strftime(" %H:%M")

	if summaryFlg == 0:
		print "detailed report"

		for noPlt in range(0,int(timeSegments)):
			print "page np" + str(noPlt)

			startTimePlot = endTimePlot
			endTimePlot = startTimePlot+5

			if endTimePlot > end_time:
				print "plot half screen"
				Sig1.sig = np.append(Sig1.sig,NaN)
				Sig1.rawSig= np.append(Sig1.rawSig,NaN)
				Sig1.t = np.append(Sig1.t,endTimePlot*60)
				t_rollin = np.append(t_rollin,endTimePlot*60)
				maxFrqs = np.append(maxFrqs,NaN)
				endTimePlot = t_rollin[-1]/60

			# pdb.set_trace()

			plotLabel.append('fig_rawSig'+str(noPlt))
			plotLabel[noPlt] = plt.figure(figsize=(11.69,8.27))

			gs1 = gridspec.GridSpec(2, 1,height_ratios=[2,1])

			plt.ax1 = plt.subplot(gs1[0])
			# plt.plot(Sig1.t/60.0, Sig1.sig)
			plt.plot(Sig1.t/60.0, Sig1.rawSig)
			plt.ylabel('Voltage (mV)')
			plt.ylim([-75,75])
			# plt.ylim([minV,maxV])
			plt.xlim([startTimePlot,endTimePlot])


			plt.ax2 = plt.subplot(gs1[1])

			plt.plot(t_rollin/60.0, maxFrqs * 60, '*r-')
			plt.ylabel('Dominant frequency (bpm)')
			# plt.ylim([frqLwrLimit,frqUprLimit])
			plt.xlim([startTimePlot,endTimePlot])

		patient_figure = plt.figure(figsize=(11.69,8.27))
		print "Getting text Info page"
		patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

		patient_text += '\n\n Signal number: %d' % np.int(dataNo+1)
		patient_text += '\nType of signal chosen: ECG'
		patient_text += '\nAutomated frequency results'

		#SUMMARY
		if ((len(validPoint)/float(len(t_rollin)))*100) != 0:
			patient_text += '\nFrequency, Mean: %3.2f bpm, Std: %3.2f bpm' % ((np.mean(maxFrqs[maxFrqs>0.3])*60), (np.std(maxFrqs[maxFrqs>0.3])*60))
		# 	patient_text +=	'\nAmplitude, Mean: %3.2f mV, Std: %3.2f mV' % (np.mean(amplValid), np.std(amplValid))
			patient_text +=	'\nSignal coverage: {:.2%}'.format(np.float(np.sum(validPoint))/len(t_rollin))
		else:
			patient_text +=	'\n No results, as signals are potentially noisy'

		ax = patient_figure.add_subplot(111)
	#	ax.get_xaxis().set_visible(False)
	#	ax.get_yaxis().set_visible(True)
		patient_figure.text(0.5, 0.5, patient_text,
						fontsize=15,
						transform=ax.transAxes.inverted().transform,
						horizontalalignment='center',
						verticalalignment='center') #, rotation = 90

		plt.tick_params(\
			axis='both',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom='off',      # ticks along the bottom edge are off
			top='off',         # ticks along the top edge are off
			left='off',         # ticks along the top edge are off
			right='off',         # ticks along the top edge are off
			labelbottom='off',labelleft='off') # labels along the bottom edge are off

		plt.ylabel(title,fontsize=8)
		plt.gca().invert_xaxis()

		#output_filename = 'Test'
		A = [patient_figure]
		A.extend(plotLabel)
		output.savePdf(A, output_file)
		plt.close("all")

		busyDlg = None
		# wx.MessageBox('Analysis Done for ECG Channel {}- Report generated'.format(dataNo+1), 'Analysis Done',wx.OK | wx.ICON_INFORMATION)

	elif summaryFlg == 1:
		print "summary report"

		patient_figure = plt.figure(figsize=(11.69,8.27))
		print "Getting text Info page"
		patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

		patient_text += '\n\n Signal number: %d' % np.int(dataNo+1)
		patient_text += '\nType of signal chosen: ECG'
		patient_text += '\nAutomated frequency results'

		#SUMMARY
		if ((len(validPoint)/float(len(t_rollin)))*100) != 0:
			patient_text += '\nFrequency, Mean: %3.2f bpm, Std: %3.2f bpm' % ((np.mean(maxFrqs[maxFrqs>0.3])*60), (np.std(maxFrqs[maxFrqs>0.3])*60))
		# 	patient_text +=	'\nAmplitude, Mean: %3.2f mV, Std: %3.2f mV' % (np.mean(amplValid), np.std(amplValid))
			patient_text +=	'\nSignal coverage: {:.2%}'.format(np.float(np.sum(validPoint))/len(t_rollin))
		else:
			patient_text +=	'\n No results, as signals are potentially noisy'

		ax = patient_figure.add_subplot(111)
	#	ax.get_xaxis().set_visible(False)
	#	ax.get_yaxis().set_visible(True)
		patient_figure.text(0.5, 0.5, patient_text,
						fontsize=15,
						transform=ax.transAxes.inverted().transform,
						horizontalalignment='center',
						verticalalignment='center') #, rotation = 90

		plt.tick_params(\
			axis='both',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom='off',      # ticks along the bottom edge are off
			top='off',         # ticks along the top edge are off
			left='off',         # ticks along the top edge are off
			right='off',         # ticks along the top edge are off
			labelbottom='off',labelleft='off') # labels along the bottom edge are off

		plt.ylabel(title,fontsize=8)
		plt.gca().invert_xaxis()

		#output_filename = 'Test'
		A = [patient_figure]
		A.extend(plotLabel)
		output.savePdf(A, output_file)
		plt.close("all")

		busyDlg = None

	elif summaryFlg == 2:
		print "return output values"
		# pdb.set_trace()
		return t_rollin, maxFrqs, validPoint

