__author__ = 'npas004'


import signal_egg as signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import pdb as pdb
import wx as wx
import datetime as datetime
from pylab import *
import findIndices as findIndices
import math as math
import matplotlib.gridspec as gridspec
from scipy import signal as scipySig

#Computing the rolling overlaps of the signals
def rolling_window_overlap(a, window, data_Overlap):
	'''
	This method seperates the signals into stacks or strides of the signal
	for computation of rolling stats. By default this method is called from
	the method 'amplitude_frequency_estimation' in this class.
	'''

	data_noOverlap = window - data_Overlap
	indices = np.arange(start=0, stop=a.size - window + 1, step=data_noOverlap)
	shape = indices.shape[-1], window
	strides = (a.strides[-1] * data_noOverlap,) + a.strides
	return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def find_nearest(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return array[idx]

def find_nearest_indx(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return idx

def calcFrqPercentage(fftFrq, fft, indx_frq01, indx_frq10):
	'''
	Estimate signal integrity measure. Here the % power of the fft signals are
	given in the range of specified intervals
	'''
	Meas = [None] * len(fft)

	for i in np.arange(0, len(fft)):
#		Meas[i] = (np.max(fft[i][indx_frq01:indx_frq10]) / np.max(fft[i])) * 100
		A=np.sum(np.abs(fft[i][indx_frq01:indx_frq10]))
		B=np.sum(np.abs(fft[i]))
		Meas[i] = ((A)/(B))*100
	return Meas

#Helper function to compute the next power of two
def nextpow2(n):
	m_f = np.log2(n)
	m_i = np.ceil(m_f)
	return (2**m_i)*20

def amplPRESSsigProc (Sig1,windowSize,overlapWindow):
	print "Amplitude estimation"
	# windowSize=	5*30
	# overlapWindow = windowSize-(2*30)
	# pdb.set_trace()
	sig_rollin = rolling_window_overlap(Sig1.sig, window=windowSize, data_Overlap=overlapWindow)
	t_rollin = rolling_window_overlap(Sig1.t,windowSize,overlapWindow)
	t_rollin = np.median(t_rollin,axis=1)

	maxSig = np.max(sig_rollin,-1)
	minSig = np.min(sig_rollin,-1)
	# meanArterialPressure = minSig  + ((maxSig - minSig)/3)
	meanArterialPressure =  maxSig - minSig

	return meanArterialPressure, maxSig, minSig, t_rollin

def amplPRESSsigProcTwiceOver (Sig1,windowSize,overlapWindow,windowSize1,overlapWindow1):
	print "Amplitude estimation"

	#First rolling window to match 1 min estimates from EGG and ECG
	sig_rollin1 = rolling_window_overlap(Sig1.sig, window = windowSize1, data_Overlap = overlapWindow1)
	t_rollin1 = rolling_window_overlap(Sig1.t,window = windowSize1, data_Overlap = overlapWindow1)
	t_rollin1 = np.median(t_rollin1,axis=1)

	# pdb.set_trace()
	Ampl_array = np.array([])
	for x in sig_rollin1:
		# print x
		sig_rollin = rolling_window_overlap(x, window=windowSize, data_Overlap=overlapWindow)
		maxSig = np.max(sig_rollin,-1)
		minSig = np.min(sig_rollin,-1)
		Ampl_array = np.append(Ampl_array , np.median(maxSig-minSig))

	# pdb.set_trace()

	return Ampl_array, t_rollin1







def freqPRESSsigProc (Sig1,windowSize,overlapWindow):
	'[freqPressure,t_rollin] = freqPRESSsigProc(Sig1,windowSize,overlapWindow)'
	print "Frequency estimation"
	# windowSize=	5*30
	# overlapWindow = windowSize-(2*30)
	sig_rollin = rolling_window_overlap(Sig1.sig, window=windowSize, data_Overlap=overlapWindow)
	t_rollin = rolling_window_overlap(Sig1.t, windowSize, overlapWindow)
	t_rollin = np.median(t_rollin, axis=1)

	sig_rollin = scipySig.detrend(sig_rollin, axis=1, type='constant')  #Detrend signal

	# 1. Window signals
	print "window signal"
	hannWind = scipySig.hann(sig_rollin.shape[1], sym=True)
	hannWind = np.tile(hannWind, [sig_rollin.shape[0], 1])
	sig_rollin = sig_rollin * hannWind

	#2. Zero padding for the fft signal
	print "zero pad signal"
	L = nextpow2(sig_rollin.shape[1])
	fourier = fft(sig_rollin, int(L))
	fourier = np.abs(fourier[0:len(fourier) ** 2, 0:L / 2])
	freqs = np.linspace(0, 15, L / 2)

	maxFrqs = freqs[fourier.argmax(axis=1)]
	# print "Frequency (bpm) for entire time - " + str(maxFrqs * 60)  #Convert to cpm

	return maxFrqs, t_rollin

def pressureSignalAnalysisReporting(patient_info, Data, output_file,dataNo, summaryFlg,versionNo):

	msg = "Blood Pressure signal {} - Please wait while we process and generate the report...".format(dataNo+1)
	busyDlg = wx.BusyInfo(msg)

	output_file = output_file[:-4]+ unicode('_signal_'+str(dataNo+1)+'.pdf')

	#Initialise test object
	Sig1 = signal_egg.Signal(sig=Data[1][:, dataNo], fs=30.0)
	print "Signal Filtering"

	#High frequency removal
	print "HF noise removal"
	lp_filt = signal_egg.lowpass_ButterFilter(filtOrder=5, cutoff=2.5)
	Sig1 = lp_filt(Sig1)
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.show()

	print "Baseline removal"
	baselineLSS = signal_egg.baseline_als(Sig1.sig, lam=1000, p=0.05, niter=10)
	# plt.plot(Sig1.t,Sig1.sig, Sig1.t, baselineLSS, Sig1.t, Sig1.sig - baselineLSS) # testing the baseline removal feature
	# plt.show()

	Sig1.sig = Sig1.sig - baselineLSS
	Sig1.sig = Sig1.sig - np.median(Sig1.sig)
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.show()


	print "Amplitude PRESSURE estimation"
	windowSize=	3*30	#30 is sampling freq
	overlapWindow = windowSize-(0.5*30)
	# [meanArterialPressure,maxSig,minSig,t_rollin] = amplPRESSsigProc(Sig1,windowSize,overlapWindow) #normal ampl calc
	windowSize1 = 1800
	overlapWindow1 = windowSize1 - 300
	[meanArterialPressure,t_rollin] = amplPRESSsigProcTwiceOver(Sig1,windowSize,overlapWindow,windowSize1,overlapWindow1)
	#ampl calc to match metrics -- doing rolling windows twice to accuate amplitude resutlts

	print "Frequency PRESSURE estimation"
	windowSize=	1800 #60*30	#30 is sampling freq
	overlapWindow = windowSize - 300 #windowSize - (20 * 30)
	[frequencyPressure,t_rollinFrq] = freqPRESSsigProc(Sig1,windowSize,overlapWindow)
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig),plt.figure(),plt.plot(t_rollinFrq,frequencyPressure,'*-')
	# plt.figure(),plt.plot(Sig1.t,Sig1.sig,t_rollin,meanArterialPressure),plt.show()
	#
	# pdb.set_trace()
	#Checking if the estimate is valid
	validPoint = [1]*len(t_rollin)
	validPoint = np.asarray(validPoint)
	validPoint[frequencyPressure*60 < 30] = 0	#Limits of frequency for ECG
	# validPoint[frequencyPressure*60 > 115] = 0	#Limits of frequency for ECG
	# validPoint = [1]*len(t_rollin)
	# validPoint = np.asarray(validPoint)
	
	#Compute metrics of interest
	# PLOTTING TO SAVE TO PDF FILE

	# -- 1.0 - get time segments of interets
	#Ask user for time limits
	time_lim = (np.min(Sig1.t) / 60, np.max(Sig1.t) / 60)
	print time_lim


	#First page with average results
	date_now = datetime.datetime.now()
	DateAnaly = 'Date Analysed: '+ date_now.strftime("%A %d. %B %Y ") +date_now.strftime("%H:%M")
#	TimeAnaly = 'Time Analysed: '+ date_now.strftime("%H:%M")
	title = 'ver-' + str(versionNo) + '--- Date Analysed: '+ date_now.strftime("%A %d. %B %Y") +date_now.strftime(" %H:%M")

	start_time = np.min(Sig1.t) / 60
	start_time_vec = (find_nearest_indx(Sig1.t, start_time * Sig1.fs))
	end_time = np.max(Sig1.t) / 60
	end_time_vec = (find_nearest_indx(Sig1.t, end_time * Sig1.fs))

	plotLabel = []
	timeSegments = ceil((end_time - start_time) / 5)  # 5 min time segments
	endTimePlot = 0
	startTimePLot = 0

	if summaryFlg == 0:
		print "detailed rep"

		for noPlt in range(0,int(timeSegments)):
			print "page np" + str(noPlt)

			startTimePlot = endTimePlot
			endTimePlot = startTimePlot+5

			if endTimePlot > end_time:
				print "plot half screen"
				Sig1.sig = np.append(Sig1.sig,NaN)
				Sig1.t = np.append(Sig1.t,endTimePlot*60)
				t_rollin = np.append(t_rollin,endTimePlot*60)
				meanArterialPressure = np.append(meanArterialPressure,NaN)
				endTimePlot = t_rollin[-1]/60

			# pdb.set_trace()

			plotLabel.append('fig_rawSig'+str(noPlt))
			plotLabel[noPlt] = plt.figure(figsize=(11.69,8.27))

			gs1 = gridspec.GridSpec(2, 1,height_ratios=[2,1])

			plt.ax1 = plt.subplot(gs1[0])
			plt.plot(Sig1.t/60.0, Sig1.sig)
			plt.ylabel('Pressure (??)')
			plt.ylim([-20,25])
			# plt.ylim([minV,maxV])
			plt.xlim([startTimePlot,endTimePlot])


			plt.ax2 = plt.subplot(gs1[1])

			plt.plot(t_rollin/60.0, meanArterialPressure, '*r-')
			plt.ylabel('Mean arterial presssure (cpm)')
			plt.ylim([-120,120])
			# plt.ylim([frqLwrLimit,frqUprLimit])
			plt.xlim([startTimePlot,endTimePlot])


		patient_figure = plt.figure(figsize=(11.69,8.27))
		print "Getting text Info page"
		patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

		patient_text += '\n\n Signal number: %d' % np.int(dataNo+1)
		patient_text += '\nType of signal chosen: Blood Pressure'
		patient_text += '\nAutomated blood pressure results'

		#SUMMARY
		if ((len(validPoint)/float(len(t_rollin)))*100) != 0:
			mapMean = np.mean(meanArterialPressure[~np.isnan(meanArterialPressure)])
			mapStd = np.std(meanArterialPressure[~np.isnan(meanArterialPressure)])
			patient_text += '\nPressure, Mean: %3.2f mmHg, Std: %3.2f mmHg' % (mapMean, mapStd)
		# 	patient_text +=	'\nAmplitude, Mean: %3.2f mV, Std: %3.2f mV' % (np.mean(amplValid), np.std(amplValid))
			patient_text +=	'\nSignal coverage: {:.2%}'.format((np.sum(validPoint)/float(len(t_rollin))))
		else:
			patient_text +=	'\n No results, as signals are potentially noisy'


		ax = patient_figure.add_subplot(111)
	#	ax.get_xaxis().set_visible(False)
	#	ax.get_yaxis().set_visible(True)
		patient_figure.text(0.5, 0.5, patient_text,
						fontsize=15,
						transform=ax.transAxes.inverted().transform,
						horizontalalignment='center',
						verticalalignment='center') #, rotation = 90

		plt.tick_params(\
			axis='both',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom='off',      # ticks along the bottom edge are off
			top='off',         # ticks along the top edge are off
			left='off',         # ticks along the top edge are off
			right='off',         # ticks along the top edge are off
			labelbottom='off',labelleft='off') # labels along the bottom edge are off

		plt.ylabel(title,fontsize=8)
		plt.gca().invert_xaxis()

		#output_filename = 'Test'
		A = [patient_figure]
		A.extend(plotLabel)
		output.savePdf(A, output_file)
		plt.close("all")

		busyDlg = None
		# wx.MessageBox('Analysis Done for Pressure Channel {}- Report generated'.format(dataNo+1), 'Analysis Done',wx.OK | wx.ICON_INFORMATION)

	elif summaryFlg ==1:
		print "summary rep"

		patient_figure = plt.figure(figsize=(11.69,8.27))
		print "Getting text Info page"
		patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

		patient_text += '\n\n Signal number: %d' % np.int(dataNo+1)
		patient_text += '\nType of signal chosen: Blood Pressure'
		patient_text += '\nAutomated frequency results'

		#SUMMARY
		if ((len(validPoint)/float(len(t_rollin)))*100) != 0:
			patient_text += '\nPressure, Mean: %3.2f mmHg, Std: %3.2f mmHg' % ((np.mean(meanArterialPressure)), (np.std(meanArterialPressure)))
		# 	patient_text +=	'\nAmplitude, Mean: %3.2f mV, Std: %3.2f mV' % (np.mean(amplValid), np.std(amplValid))
			patient_text +=	'\nSignal coverage: {:.2%}'.format((np.sum(validPoint)/float(len(t_rollin))))
		else:
			patient_text +=	'\n No results, as signals are potentially noisy'

		ax = patient_figure.add_subplot(111)
	#	ax.get_xaxis().set_visible(False)
	#	ax.get_yaxis().set_visible(True)
		patient_figure.text(0.5, 0.5, patient_text,
						fontsize=15,
						transform=ax.transAxes.inverted().transform,
						horizontalalignment='center',
						verticalalignment='center') #, rotation = 90

		plt.tick_params(\
			axis='both',          # changes apply to the x-axis
			which='both',      # both major and minor ticks are affected
			bottom='off',      # ticks along the bottom edge are off
			top='off',         # ticks along the top edge are off
			left='off',         # ticks along the top edge are off
			right='off',         # ticks along the top edge are off
			labelbottom='off',labelleft='off') # labels along the bottom edge are off

		plt.ylabel(title,fontsize=8)
		plt.gca().invert_xaxis()

		#output_filename = 'Test'
		A = [patient_figure]
		A.extend(plotLabel)
		output.savePdf(A, output_file)
		plt.close("all")

		busyDlg = None

	elif summaryFlg ==2:
		print "return values"
		return t_rollin,meanArterialPressure, frequencyPressure,validPoint




