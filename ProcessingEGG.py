
import signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import pdb
import wx as wx
import datetime as datetime
from pylab import *
import findIndices as findIndices

def find_nearest(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return array[idx]

def find_nearest_indx(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return idx

def calcFrqPercentage(fftFrq, fft, indx_frq01, indx_frq10):
	'''
	Estimate signal integrity measure. Here the % power of the fft signals are 
	given in the range of specified intervals
	'''
	Meas = [None] * len(fft)

	for i in np.arange(0, len(fft)):
#		Meas[i] = (np.max(fft[i][indx_frq01:indx_frq10]) / np.max(fft[i])) * 100
		A=np.sum(np.abs(fft[i][indx_frq01:indx_frq10]))
		B=np.sum(np.abs(fft[i]))
		Meas[i] = ((A)/(B))*100
	return Meas

def signalProcAnalysis (patient_info, Data, output_file):
	'''
	Processing of the input data 
    '''
	# Need to check which are the EGG signals
	A = {}
	B=[]
	for i in Data[0]:
		A[i] = Data[0][i].get('Type')
		print A[i]
		B.append(A[i])
	print B

	#Ask user for which electrode to perform analysis
	box = wx.SingleChoiceDialog(None,'List choice of electrode for analysis','Electrode Choice',B)
	if box.ShowModal() == wx.ID_OK:
		answer = box.GetStringSelection()
		elec = box.GetSelection()
		print answer
		print elec
		print "Electrode chosen"
		box.Destroy()
		
	#Initialise test object
	Sig1 = signal_egg.Signal(sig=Data[1][:, elec], fs=30.0)

	#Ask user for time limits 
	time_lim = (np.min(Sig1.t)/60,np.max(Sig1.t)/60)
	print time_lim

	box1 = wx.TextEntryDialog(None,'Enter time (in minutes) to display EGG data segment',"Cutaneous EGG analysis",str(time_lim))
	if box1.ShowModal() == wx.ID_OK:
		answer1 = box1.GetValue()
#		print answer1
#		print type(answer1)
		AA=answer1.split(",");
#		print float(AA[0][1:])
#		print float(AA[1][:-1])
		start_time = float(AA[0][1:])
		start_time_vec = (find_nearest_indx(Sig1.t, start_time*Sig1.fs))
		end_time = float(AA[1][:-1])
		end_time_vec = (find_nearest_indx(Sig1.t, end_time*Sig1.fs))
#		print AA
#		print type(start_time)
#		print type(end_time)
#		print "start time vec "  + str(start_time_vec)
#		print "end time vec "  + str(end_time_vec)  
		box1.Destroy()
		
	fig_rawSig = plt.figure(figsize=(8, 10))
	plt.ax = plt.subplot(111)
	plt.plot(Sig1.t/60.0, Sig1.sig)		#Convert to minutes
	plt.title('Raw signal')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Voltage(mV)')
	plt.xlim([start_time,end_time])
#	plt.show()

	plt.ax.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax.yaxis.set_major_locator(MultipleLocator(0.25))
	plt.ax.yaxis.set_minor_locator(MultipleLocator(0.125))
	plt.ax.xaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	plt.ax.yaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	
	print "Signal Filtering"
	
	#Baseline removal
	print "Baseline removal"
	bas_filt = signal_egg.create_baseline_filter(median_window=20)
	Sig1 = bas_filt(Sig1)
	
	#High frequency removal
	print "HF noise removal"
	lp_filt = signal_egg.lowpass_filter(filtOrder=5, cutoff=1.5)
	Sig1 = lp_filt(Sig1)

	print "Amplitude and frequency estimation"
	#Amplitude and frequency estimation and the FAR
	sig_rollin = signal_egg.rolling_window_overlap(Sig1.sig, window=3600, data_Overlap=3600-300)
	ampl_frq_calc = signal_egg.amplitude_frequency_estimation(window=3600, data_Overlap=3600-300)
	[maxfr, maxy, mini, ampl, t, fourier, freqs, frqPeaksOneTen] = ampl_frq_calc(Sig1)
    
	#Masking signal
	# Sig1 = signal_egg.mask_bad_sections(signal=Sig1, sig_min=-100, sig_max=100, expand_points=10)
	# [ampl,maxfr] = signal_egg.mask_ampl_frq(ampl,maxfr, ampl_min = -100, ampl_max = 100, freq_min = 2, freq_max = 20)


	FAR = maxfr / ampl
	
	# Choosing valid times from the signals freq and amplitude estimate
	validPltPoint = [0]*len(maxfr)
	validFrqPoint = [0]*len(maxfr)
	validAmplPoint = [0]*len(maxfr)
	validDomFrqPoint = [0]*len(maxfr)
	for i in range(len(t)):
		if ( (ampl[i] < 0.25) and (maxfr[i]*60.0 < 10) and (maxfr[i]*60.0 > 0.9) and (frqPeaksOneTen[i]) ) == 1:
			validPoint ='Yes'
			validPltPoint[i]=1
		else:
			validPoint ='No'
			validPltPoint[i]=0
			if (ampl[i] > 0.25):
				validAmplPoint[i]=1
			if (maxfr[i]*60.0 > 10) and (maxfr[i]*60.0 < 0.9):
				validFrqPoint[i]=1
			if frqPeaksOneTen[i] > 1:
				validDomFrqPoint[i] =1
				
				

	#Removing outliers - values that change only for one segment
	difChk = (np.diff(validPltPoint))
	for i in range(len(difChk)-1):
			if difChk[i] == 1 and difChk[i+1] == -1:
				print "outlier of good signal in bad section of sigs"
				validPltPoint[i+1]=0	


	#Writing values to text file
	print "Printing out results"
	print output_file[:-4]+'.txt'
	text_file = open(output_file[:-4]+'.txt', "w")
	text_file.write("Time (min) \t Amplitude \t Frequency(cpm)"
				" \t No of Frequency Peak \t Valid Point (Yes(1)/No(0))"
				" \t Ampl Limit \t Frq Limit \t Dominant Freq Limit \n")

	amplValid = []
	freqValid = []
	validTime=0
	for i in range(len(t)):
		if validPltPoint[i]==1:
			amplValid.append(ampl[i])
			freqValid.append(maxfr[i])
			validTime +=1

		text_file.write("%3.2f \t %3.5f \t %3.2f \t %d \t %s \t %s \t %s \t %s \n" \
					 % (t[i]/60, ampl[i],maxfr[i]*60,frqPeaksOneTen[i],validPltPoint[i],validAmplPoint[i],validFrqPoint[i],validDomFrqPoint[i]))
		
	text_file.write("\n \n \nAverage coverage of valid values: \t %3.2f \t %% \n" % ((validTime/float(len(t)))*100))
	text_file.write("\n \nAverage (mean) amplitude: \t %3.5f  \t millivolts " % np.mean(amplValid))
	text_file.write("\nAverage (std) amplitude: \t %3.5f \t millivolts " % np.std(amplValid))
	text_file.write("\n \nAverage (mean) frequency: \t %3.2f \t cycles per minute" % (np.mean(freqValid)*60))
	text_file.write("\nAverage (std) frequency:\t %3.2f \tcycles per minute" % (np.std(freqValid)*60))
	
	text_file.close()

	
	#Estimate some sort of signal quality measure - sum Frq in range 1 to 10 cpm 
	indx_frq01 = int(find_nearest_indx(freqs, 1/60.0))
	indx_frq10 = int(find_nearest_indx(freqs, 10/60.0))
	Meas = calcFrqPercentage(freqs, fourier, indx_frq01, indx_frq10)

	#Plotting the filtered sinal 
	minV = -0.125
	maxV = 0.25
	fig_filtSig = plt.figure(figsize=(8, 10))
	plt.ax=subplot(111)
	plt.plot(Sig1.t/60.0, Sig1.sig)
   	plt.title('Filtered signal')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Voltage(mV)')
	plt.xlim([start_time,end_time])
	plt.ylim([minV,maxV])
	
	plt.ax.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax.yaxis.set_major_locator(MultipleLocator(0.125))
	plt.ax.yaxis.set_minor_locator(MultipleLocator(0.125))
	plt.ax.xaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	plt.ax.yaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	
	#Plotting the filtered signal and amplitude
	fig_filtSigAmpl = plt.figure(figsize=(8, 10))
	plt.ax=subplot(111)
	plt.plot(Sig1.t/60.0, Sig1.sig, t/60.0, ampl, '-*g')
   	plt.title('Filtered signal and amplitude')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Voltage(mV)')
	plt.xlim([start_time,end_time])
	plt.ylim([minV,maxV])
		
	plt.ax.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax.yaxis.set_major_locator(MultipleLocator(0.125))
	plt.ax.yaxis.set_minor_locator(MultipleLocator(0.125))
	plt.ax.xaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	plt.ax.yaxis.grid(True,'major',linewidth=1)
	plt.ax.xaxis.grid(True,'minor',linewidth=1)
	
	#Plotting the signal and dominant frequency
	fig_filtSigFrq = plt.figure(figsize=(8, 10))
#	ax1 = fig_filtSigFrq.add_subplot(211)
	plt.ax1=plt.subplot(211)
	plt.plot(Sig1.t/60.0, Sig1.sig)
   	plt.title('Filtered signal and frequency')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Voltage(mV)')
	plt.xlim([start_time,end_time])
	plt.ylim([minV,maxV])
	
	plt.ax1.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax1.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax1.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax1.yaxis.set_major_locator(MultipleLocator(0.125))
	plt.ax1.yaxis.set_minor_locator(MultipleLocator(0.125))
	plt.ax1.xaxis.grid(True,'major',linewidth=1)
	plt.ax1.xaxis.grid(True,'minor',linewidth=1)
	plt.ax1.yaxis.grid(True,'major',linewidth=1)
	plt.ax1.xaxis.grid(True,'minor',linewidth=1)

#	ax2 = fig_filtSigFrq.add_subplot(212, sharex=ax1)
	frqLwrLimit=0
	frqUprLimit=10
	plt.ax2=plt.subplot(212,sharex=plt.ax1)
	plt.plot(t/60.0, maxfr * 60, '*r-')
   	plt.title('Dominant frequency')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Frequency (cpm)')
	plt.xlim([start_time,end_time])
	plt.ylim([frqLwrLimit,frqUprLimit])
	
	plt.ax2.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax2.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax2.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax2.yaxis.set_major_locator(MultipleLocator(1))
	plt.ax2.yaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax2.xaxis.grid(True,'major',linewidth=1)
	plt.ax2.xaxis.grid(True,'minor',linewidth=1)
	plt.ax2.yaxis.grid(True,'major',linewidth=1)
	plt.ax2.xaxis.grid(True,'minor',linewidth=1)

	#	pdb.set_trace()
	
	#Plotting the signal and signal integrity  metric
	fig_filtSigMeas = plt.figure(figsize=(8, 10))
#	ax1 = fig_filtSigMeas.add_subplot(211)
	plt.ax1=plt.subplot(211)
	plt.plot(Sig1.t/60.0, Sig1.sig)
   	plt.title('Filtered signal and signal integrity measure')
	plt.xlabel('Time(minutes)')
	plt.ylabel('Voltage(mV)')
	plt.xlim([start_time,end_time])
	plt.ylim([minV,maxV])

	plt.ax1.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax1.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax1.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	plt.ax1.yaxis.set_major_locator(MultipleLocator(0.125))
	plt.ax1.yaxis.set_minor_locator(MultipleLocator(0.125))
	plt.ax1.xaxis.grid(True,'major',linewidth=1)
	plt.ax1.xaxis.grid(True,'minor',linewidth=1)
	plt.ax1.yaxis.grid(True,'major',linewidth=1)
	plt.ax1.xaxis.grid(True,'minor',linewidth=1)

	
#	ax2 = fig_filtSigMeas.add_subplot(212, sharex=ax1)
	plt.ax2=plt.subplot(212, sharex=plt.ax1)
#	plt.plot(t/60.0, frqPeaksOneTen, '*-')  
#	plt.title('No of dominant frequency peaks in in 1-10 cycles per minute')
#	plt.xlabel('Time(minutes)')
#	plt.ylabel('No of peaks')
#	plt.xlim([start_time,end_time])
#	plt.ylim([0,5])	

	plt.title('Signal integreity metric')
	plt.plot(t/60.0,validPltPoint)
	plt.yticks([0, 1], ['Noisy\n segment', 'Good\n signal'])
	plt.ylim([-0.5,1.5])
	plt.xlabel('Time(minutes)')
	plt.xlim([start_time,end_time])
	
	#plt.plot(t/60.0, Meas, '*-')  
	#plt.title('Percentage power of signals in 1-10 cycles per minute')
	#plt.xlabel('Time(minutes)')
	#plt.ylabel('% signal integrity')
	#plt.xlim([start_time,end_time])
	#plt.ylim([0,110])
	
	plt.ax2.xaxis.set_major_locator(MultipleLocator(1))
	plt.ax2.xaxis.set_minor_locator(MultipleLocator(0.5))
	plt.ax2.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
	#plt.ax2.yaxis.set_major_locator(MultipleLocator(20))
	#plt.ax2.yaxis.set_minor_locator(MultipleLocator(10))
	plt.ax2.yaxis.set_major_locator(MultipleLocator(1))
	plt.ax2.yaxis.set_minor_locator(MultipleLocator(1))
	plt.ax2.xaxis.grid(True,'major',linewidth=1)
	plt.ax2.xaxis.grid(True,'minor',linewidth=1)
	plt.ax2.yaxis.grid(True,'major',linewidth=1)
	plt.ax2.xaxis.grid(True,'minor',linewidth=1)

	
	
	
	date_now = datetime.datetime.now()
	DateAnaly = 'Date Analysed: '+ date_now.strftime("%A %d. %B %Y ") +date_now.strftime("%H:%M")
#	TimeAnaly = 'Time Analysed: '+ date_now.strftime("%H:%M")
	versionNo = '20130502'
	title = 'ver-' + versionNo + '--- Date Analysed: '+ date_now.strftime("%A %d. %B %Y") +date_now.strftime(" %H:%M")
	
	patient_figure = plt.figure(figsize=[10, 10])
	print "Getting text Info page"
	patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

	patient_text += '\n\n Automated frequency amplitude results \nFrequency, Mean: %3.2f cpm, Std: %3.2f cpm \nAmplitude, Mean: %3.2f mV, Std: %3.2f mV \nSignal coverage: %3.2f %%' % ( (np.mean(freqValid)*60), (np.std(freqValid)*60), np.mean(amplValid), np.std(amplValid), (validTime/float(len(t)))*100 )


	print "Finish getting text info"
	ax = patient_figure.add_subplot(111)
	ax.get_xaxis().set_visible(False)
	ax.get_yaxis().set_visible(False)
	patient_figure.text(0.5, 0.5, patient_text, 
					fontsize=15, 
					transform=ax.transAxes,
					horizontalalignment='center',
					verticalalignment='center')
	plt.title(title)
	
	#Print output signals to a pdf
	A = [patient_figure, fig_rawSig, fig_filtSig, fig_filtSigAmpl, fig_filtSigFrq, fig_filtSigMeas]
	#output_filename = 'Test'
	output.savePdf(A, output_file)
	
	box2 = wx.MessageDialog(None,'EGG analysis performed and signal plots are printed to pdf. \
	Amplitude and frequency values are written to a text file.',"Cutaneous EGG analysis",wx.OK)
	if box2.ShowModal()==wx.ID_OK:
		box2.Destroy()

	#plt.show()

