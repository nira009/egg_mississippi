
import signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import pdb
import wx as wx
import datetime as datetime
from pylab import *
import findIndices as findIndices
import math as math
import matplotlib.gridspec as gridspec

def find_nearest(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return array[idx]

def find_nearest_indx(array, value):
	'''
	Helper function to find the nearest value in an array
	'''
	idx = (np.abs(array - value)).argmin()
	return idx

def calcFrqPercentage(fftFrq, fft, indx_frq01, indx_frq10):
	'''
	Estimate signal integrity measure. Here the % power of the fft signals are 
	given in the range of specified intervals
	'''
	Meas = [None] * len(fft)

	for i in np.arange(0, len(fft)):
#		Meas[i] = (np.max(fft[i][indx_frq01:indx_frq10]) / np.max(fft[i])) * 100
		A=np.sum(np.abs(fft[i][indx_frq01:indx_frq10]))
		B=np.sum(np.abs(fft[i]))
		Meas[i] = ((A)/(B))*100
	return Meas

def signalProcAnalysis (patient_info, Data, output_file):
	'''
	Processing of the input data 
    '''
	# Need to check which are the EGG signals
	A = {}
	B=[]
	for i in Data[0]:
		A[i] = Data[0][i].get('Type')
		print A[i]
		B.append(A[i])
	print B

	#Ask user for which electrode to perform analysis
	box = wx.SingleChoiceDialog(None,'List choice of electrode for analysis','Electrode Choice',B)
	if box.ShowModal() == wx.ID_OK:
		answer = box.GetStringSelection()
		elec = box.GetSelection()
		print answer
		print elec
		print "Electrode chosen"
		box.Destroy()
		
	#Initialise test object
	Sig1 = signal_egg.Signal(sig=Data[1][:, elec], fs=30.0)

	#Ask user for time limits 
	time_lim = (np.min(Sig1.t)/60,np.max(Sig1.t)/60)
	print time_lim

	box1 = wx.TextEntryDialog(None,'Enter time (in minutes) to display EGG data segment',"Cutaneous EGG analysis",str(time_lim))
	if box1.ShowModal() == wx.ID_OK:
		answer1 = box1.GetValue()
#		print answer1
#		print type(answer1)
		AA=answer1.split(",");
#		print float(AA[0][1:])
#		print float(AA[1][:-1])
		start_time = float(AA[0][1:])
		start_time_vec = (find_nearest_indx(Sig1.t, start_time*Sig1.fs))
		end_time = float(AA[1][:-1])
		end_time_vec = (find_nearest_indx(Sig1.t, end_time*Sig1.fs))
#		print AA
#		print type(start_time)
#		print type(end_time)
#		print "start time vec "  + str(start_time_vec)
#		print "end time vec "  + str(end_time_vec)  
		box1.Destroy()
		

	print "Signal Filtering"
	
	#Baseline removal
	print "Baseline removal"
	bas_filt = signal_egg.create_baseline_filter(median_window=20)
	Sig1 = bas_filt(Sig1)
	
	#High frequency removal
	print "HF noise removal"
	lp_filt = signal_egg.lowpass_filter(filtOrder=5, cutoff=1.5)
	Sig1 = lp_filt(Sig1)

	print "Amplitude and frequency estimation"
	#Amplitude and frequency estimation and the FAR
	sig_rollin = signal_egg.rolling_window_overlap(Sig1.sig, window=3600, data_Overlap=3600-300)
	ampl_frq_calc = signal_egg.amplitude_frequency_estimation(window=3600, data_Overlap=3600-300)
	[maxfr, maxy, mini, ampl, t, fourier, freqs, frqPeaksOneTen] = ampl_frq_calc(Sig1)
    
	#Masking signal
	# Sig1 = signal_egg.mask_bad_sections(signal=Sig1, sig_min=-100, sig_max=100, expand_points=10)
	# [ampl,maxfr] = signal_egg.mask_ampl_frq(ampl,maxfr, ampl_min = -100, ampl_max = 100, freq_min = 2, freq_max = 20)


	FAR = maxfr / ampl
	
	# Choosing valid times from the signals freq and amplitude estimate
	validPltPoint = [0]*len(maxfr)
	validFrqPoint = [0]*len(maxfr)
	validAmplPoint = [0]*len(maxfr)
	validDomFrqPoint = [0]*len(maxfr)
	for i in range(len(t)):
		if ( (ampl[i] < 0.25) and (maxfr[i]*60.0 < 10) and (maxfr[i]*60.0 > 0.9) and (frqPeaksOneTen[i]) ) == 1:
			validPoint ='Yes'
			validPltPoint[i]=1
		else:
			validPoint ='No'
			validPltPoint[i]=0
			if (ampl[i] > 0.25):
				validAmplPoint[i]=1
			if (maxfr[i]*60.0 > 10) and (maxfr[i]*60.0 < 0.9):
				validFrqPoint[i]=1
			if frqPeaksOneTen[i] > 1:
				validDomFrqPoint[i] =1
				
				

	#Removing outliers - values that change only for one segment
	difChk = (np.diff(validPltPoint))
	for i in range(len(difChk)-1):
			if difChk[i] == 1 and difChk[i+1] == -1:
				print "outlier of good signal in bad section of sigs"
				validPltPoint[i+1]=0	

	# Plotting data after filtering
	minV = -0.125
	maxV = 0.25
	frqLwrLimit=0
	frqUprLimit=10
		
	plotLabel=[]
	timeSegments = ceil((end_time - start_time) / 5) # 5 min time segments
	endTimePlot = 0
	startTimePLot =0
	for noPlt in range(0,int(timeSegments)): 
		
		startTimePlot = endTimePlot;
		endTimePlot = startTimePlot+5;
		if endTimePlot > end_time:
			endTimePlot = end_time
					
		plotLabel.append('fig_rawSig'+str(noPlt))		
#		fig_rawSig = plt.figure(figsize=(8, 10))
		plotLabel[noPlt] = plt.figure(figsize=(8, 10))
		
		gs1 = gridspec.GridSpec(1, 3,width_ratios=[3,2,1])

#		ax1 = plt.subplot(gs1[0])
#		ax2 = plt.subplot(gs1[1])
#		ax3 = plt.subplot(gs1[2])


#		plt.ax = plt.subplot(121)
		plt.ax1 = plt.subplot(gs1[0])
		
#		plt.plot(Sig1.t/60.0, Sig1.sig, t/60.0, ampl, '-*g')
		plt.plot(Sig1.sig,Sig1.t/60.0, ampl,t/60.0, '-*g')
	   	plt.title('Filtered signal and amplitude')
		plt.ylabel('Time(minutes)')
		plt.xlabel('Voltage(mV)')
#		plt.xlim([start_time,end_time])
		plt.xlim([minV,maxV])
			
		plt.ax1.yaxis.set_major_locator(MultipleLocator(1))
		plt.ax1.yaxis.set_minor_locator(MultipleLocator(0.5))
		plt.ax1.yaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
		plt.ax1.xaxis.set_major_locator(MultipleLocator(0.125))
		plt.ax1.xaxis.set_minor_locator(MultipleLocator(0.125))
		plt.ax1.yaxis.grid(True,'major',linewidth=1)
		plt.ax1.yaxis.grid(True,'minor',linewidth=1)
		plt.ax1.xaxis.grid(True,'major',linewidth=1)
		plt.ax1.yaxis.grid(True,'minor',linewidth=1)
		
		plt.ylim([startTimePlot,endTimePlot])	
		plt.gca().invert_xaxis()
		plt.ax1.yaxis.tick_right()
		plt.xticks(rotation=90)
		plt.yticks(rotation=90)
	
	#===========================================================================
	#	plt.plot(Sig1.sig, Sig1.t/60.0)		#Convert to minutes
	#	plt.title('Raw signal')
	#	plt.ylabel('Time(minutes)')
	#	plt.xlabel('Voltage(mV)')
	# # 	plt.ylim([start_time,end_time])
	#	plt.ylim([startTimePlot,endTimePlot])	
	#	plt.gca().invert_xaxis()
	#	plt.ax1.yaxis.tick_right()
	#	plt.xticks(rotation=90)
	#	plt.yticks(rotation=90)
	#===========================================================================
		
		plt.ax2 = plt.subplot(gs1[1])
		plt.plot(maxfr * 60, t/60.0, '*r-')
	   	plt.title('Dominant frequency')
		plt.ylabel('Time(minutes)')
		plt.xlabel('Frequency (cpm)')
#		plt.ylim([start_time,end_time])
		plt.xlim([frqLwrLimit,frqUprLimit])
		
		plt.ax2.yaxis.set_major_locator(MultipleLocator(1))
		plt.ax2.yaxis.set_minor_locator(MultipleLocator(0.5))
		plt.ax2.yaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
		plt.ax2.xaxis.set_major_locator(MultipleLocator(1))
		plt.ax2.xaxis.set_minor_locator(MultipleLocator(0.5))
		plt.ax2.yaxis.grid(True,'major',linewidth=1)
		plt.ax2.yaxis.grid(True,'minor',linewidth=1)
		plt.ax2.xaxis.grid(True,'major',linewidth=1)
		plt.ax2.yaxis.grid(True,'minor',linewidth=1)

		plt.ylim([startTimePlot,endTimePlot])	
		plt.gca().invert_xaxis()
		plt.ax2.yaxis.tick_right()
		plt.xticks(rotation=90)
		plt.yticks(rotation=90)			
		
		#===========================================================================
		#===========================================================================
		
		plt.ax3 = plt.subplot(gs1[2])
		plt.title('Signal integreity metric')
		plt.plot(validPltPoint, t/60.0)
		plt.xticks([0, 1], ['Noisy\n segment', 'Good\n signal'])
		plt.xlim([-0.5,1.5])
		plt.ylabel('Time(minutes)')
		
#		plt.ax3.xaxis.set_major_locator(MultipleLocator(1))
#		plt.ax3.xaxis.set_minor_locator(MultipleLocator(0.5))
#		plt.ax3.xaxis.set_major_formatter(FormatStrFormatter('%3.2f'))
#		#plt.ax3.yaxis.set_major_locator(MultipleLocator(20))
#		#plt.ax3.yaxis.set_minor_locator(MultipleLocator(10))
#		plt.ax3.yaxis.set_major_locator(MultipleLocator(1))
#		plt.ax3.yaxis.set_minor_locator(MultipleLocator(1))
#		plt.ax3.xaxis.grid(True,'major',linewidth=1)
#		plt.ax3.xaxis.grid(True,'minor',linewidth=1)
#		plt.ax3.yaxis.grid(True,'major',linewidth=1)
#		plt.ax3.xaxis.grid(True,'minor',linewidth=1)
						
		plt.ylim([startTimePlot,endTimePlot])	
		plt.gca().invert_xaxis()
		plt.ax3.yaxis.tick_right()
		plt.xticks(rotation=90)
		plt.yticks(rotation=90)		
		

		#===========================================================================
		#===========================================================================
		
	#Writing values to text file
	print "Printing out results"
	print output_file[:-4]+'.txt'
	text_file = open(output_file[:-4]+'.txt', "w")
	text_file.write("Time (min) \t Amplitude \t Frequency(cpm)"
				" \t No of Frequency Peak \t Valid Point (Yes(1)/No(0))"
				" \t Ampl Limit \t Frq Limit \t Dominant Freq Limit \n")

	amplValid = []
	freqValid = []
	validTime=0
	for i in range(len(t)):
		if validPltPoint[i]==1:
			amplValid.append(ampl[i])
			freqValid.append(maxfr[i])
			validTime +=1

		text_file.write("%3.2f \t %3.5f \t %3.2f \t %d \t %s \t %s \t %s \t %s \n" \
					 % (t[i]/60, ampl[i],maxfr[i]*60,frqPeaksOneTen[i],validPltPoint[i],validAmplPoint[i],validFrqPoint[i],validDomFrqPoint[i]))
		
	text_file.write("\n \n \nAverage coverage of valid values: \t %3.2f \t %% \n" % ((validTime/float(len(t)))*100))
	text_file.write("\n \nAverage (mean) amplitude: \t %3.5f  \t millivolts " % np.mean(amplValid))
	text_file.write("\nAverage (std) amplitude: \t %3.5f \t millivolts " % np.std(amplValid))
	text_file.write("\n \nAverage (mean) frequency: \t %3.2f \t cycles per minute" % (np.mean(freqValid)*60))
	text_file.write("\nAverage (std) frequency:\t %3.2f \tcycles per minute" % (np.std(freqValid)*60))
	
	text_file.close()

	
	#Estimate some sort of signal quality measure - sum Frq in range 1 to 10 cpm 
	indx_frq01 = int(find_nearest_indx(freqs, 1/60.0))
	indx_frq10 = int(find_nearest_indx(freqs, 10/60.0))
	Meas = calcFrqPercentage(freqs, fourier, indx_frq01, indx_frq10)


	
	date_now = datetime.datetime.now()
	DateAnaly = 'Date Analysed: '+ date_now.strftime("%A %d. %B %Y ") +date_now.strftime("%H:%M")
#	TimeAnaly = 'Time Analysed: '+ date_now.strftime("%H:%M")
	versionNo = '20130502'
	title = 'ver-' + versionNo + '--- Date Analysed: '+ date_now.strftime("%A %d. %B %Y") +date_now.strftime(" %H:%M")
	
	patient_figure = plt.figure(figsize=[8, 10])
	print "Getting text Info page"
	patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

	patient_text += '\n\n Automated frequency amplitude results \nFrequency, Mean: %3.2f cpm, Std: %3.2f cpm \nAmplitude, Mean: %3.2f mV, Std: %3.2f mV \nSignal coverage: %3.2f %%' % ( (np.mean(freqValid)*60), (np.std(freqValid)*60), np.mean(amplValid), np.std(amplValid), (validTime/float(len(t)))*100 )


	print "Finish getting text info"
	ax = patient_figure.add_subplot(111)
#	ax.get_xaxis().set_visible(False)
#	ax.get_yaxis().set_visible(True)
	patient_figure.text(0.5, 0.5, patient_text, 
					fontsize=15, 
					transform=ax.transAxes.inverted().transform,
					horizontalalignment='center',
					verticalalignment='center', rotation = 90)

	plt.tick_params(\
	    axis='both',          # changes apply to the x-axis
	    which='both',      # both major and minor ticks are affected
	    bottom='off',      # ticks along the bottom edge are off
	    top='off',         # ticks along the top edge are off
	    left='off',         # ticks along the top edge are off
	    right='off',         # ticks along the top edge are off
	    labelbottom='off',labelleft='off') # labels along the bottom edge are off


	plt.ylabel(title,fontsize=8)
	plt.gca().invert_xaxis()
#	plt.show()
	
	#Print output signals to a pdf
	#===========================================================================
	# A = [patient_figure, fig_rawSig, fig_filtSig, fig_filtSigAmpl, fig_filtSigFrq, fig_filtSigMeas]
	#===========================================================================
	#output_filename = 'Test'
	A = [patient_figure]
	A.extend(plotLabel)
	output.savePdf(A, output_file)
	

	
	box2 = wx.MessageDialog(None,'EGG analysis performed and signal plots are printed to pdf. \
	Amplitude and frequency values are written to a text file.',"Cutaneous EGG analysis",wx.OK)
	if box2.ShowModal()==wx.ID_OK:
		box2.Destroy()

	#plt.show()

