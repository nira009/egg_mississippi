
import signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import pdb
import wx as wx
import datetime as datetime
from pylab import *
import findIndices as findIndices
import math as math
import matplotlib.gridspec as gridspec

import processGoodSegmentAverage as processRep

import EGG_signalAnalysisReporting as sigEggProcRep
import ECG_signalAnalysisReporting as sigEcgProcRep
import PRESSURE_signalAnalysisReporting as sigPressProcRep



def signalProcAnalysis (patient_info, Data, output_file):
	'''
	Processing of the input data 
    '''
	# Need to check which are the EGG signals
	A = {}
	B=[]

	#MAIN LOOP to go over all the signals and perform apporiate analysis
	for i in Data[0]:
		A[i] = Data[0][i].get('Type')
		print A[i]
		B.append(A[i])

		if A[i] == 'CHTYPE_EGG':
			print "EGG signal"
			# pdb.set_trace()
			# #Perform analysis and reporting
			# # pdb.set_trace()
			# sigEggProcRep.eggSignalAnalysisReporting(patient_info, Data, output_file,i)
			#
			# msgAfterCompAnaly = r"""Signal """ + str(i+1)+ \
			# 					""" - EGG analysis performed and signal plots have been saved to a pdf file."""\
			# 					"""\n Amplitude and frequency values are written to a text file."""
			#
			# box3 = wx.MessageDialog(None, msgAfterCompAnaly, "Cutaneous EGG analysis - Signal "+str(i+1), wx.OK)
			# if box3.ShowModal()==wx.ID_OK:
			# 	box3.Destroy()

		elif A[i] == 'CHTYPE_RESP':
			print "RESPIRATION"
			# pdb.set_trace()
			# plt.plot(Data[1][:,i])
			# plt.show()
			# pdb.set_trace()

		elif A[i] == 'CHTYPE_ECG':
			print "ECG SIGNAL"
			sigEcgProcRep.ecgSignalAnalysisReporting(patient_info, Data, output_file,i)
			# pdb.set_trace()
			# plt.plot(Data[1][:,i])
			# plt.show()
			# pdb.set_trace()

		elif A[i] == 'CHTYPE_PRESS':
			print "PRESSURE"
			# pdb.set_trace()
			sigPressProcRep.pressureSignalAnalysisReporting(patient_info, Data, output_file,i)
			# # pdb.set_trace()
			# plt.plot(Data[1][:,i])
			# plt.show()
			# pdb.set_trace()

	# pdb.set_trace()
	box4 = wx.MessageDialog(None, "Data analysis completed", "Cutaneous EGG analysis", wx.OK)
	if box4.ShowModal()==wx.ID_OK:
		box4.Destroy()
