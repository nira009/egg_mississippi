__author__ = 'npas004'

import pyparsing as py
import pdb
import re
from pyparsing import Word, Literal, alphas, Combine
import sandhill_input

def getEvents(config_filename):

	with open(config_filename, 'rb') as f:
		conf = sandhill_input.read_conf(f)

	#Acquire events
	pattern = 'Event'
	regexp = re.compile(pattern)
	EventListDict=[]

	indx = len(conf)-1
	for i in range(0,len(conf[indx])): #Event data
		match = regexp.search(conf[indx][i][0])
		if match:
			# print conf[7][i]
			EventListDict.append(conf[indx][i])

	pattern1 = 'Time'
	regexp1 = re.compile(pattern1)
	for i in range(0,len(conf[indx-1])): #Patient data
		match = regexp1.search(conf[indx-1][i][0])
		if match:
			# print conf[6][i]
			EventListDict.append(conf[indx-1][i]) #Perhaps initial time? to check

	return EventListDict

if __name__ == '__main__':
	config_filename = u'D:\\Users\\npas004\\mshUpdatedGitPyCh\\egg_mississippi\\RenamedFiles\\Patient70\\E KNIGHT (ACEM), ERIC 11-08-2013 00.c00'
	print getEvents(config_filename)
