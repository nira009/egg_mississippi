'''
Created on 21/03/2013

@author: npas004
'''


def indices(a, func):
    return [i for (i, val) in enumerate(a) if func(val)]
