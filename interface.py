'''
Created on 11/07/2012

@author: npas004, sbul028
'''
import wx
import os
import sandhill_input
# import ProcessingEGG_v2 as ProcessingEGG	# Performs signal processing and analysis
#import ProcessingEGG_v3 as ProcessingEGG	# Report format is only the filtered signal
#import ProcessingEGG_v4 as ProcessingEGG	# Reports format is filtered signal and bad and good sections
# import ProcessingEGG_v2_v0 as ProcessingEGG	# Performs signal processing and analysis - Loop through all signals
import EGG_signalAnalysisReporting as sigEggProcRep
import ECG_signalAnalysisReporting as sigEcgProcRep
import PRESSURE_signalAnalysisReporting as sigPressProcRep
import pdb as pdb
from functools import partial as partial
import signalAnalysisACEM as acemSig
import textwrap as textwrap
from pydispatch import dispatcher


class OtherFrame(wx.Frame):
	""""""

	#----------------------------------------------------------------------
	def __init__(self):
		"""Constructor"""
		# wx.Frame.__init__(self, None, wx.ID_ANY, "Secondary Frame")
		wx.Frame.__init__(self,None,title="Set justifiable frequency limits for EGG analysis",size=(320,190),style = wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
		panel2 = wx.Panel(self)

		freqLow=1
		freqTop=10

		basicLabel = wx.StaticText(panel2, -1, "Lower frequency limit (cpm):")
		self.LowerFrqTxt = wx.TextCtrl(panel2, -1, "%3.2f" % (freqLow), size=(175, -1))
		# self.basicText.SetInsertionPoint(0)
		pwdLabel = wx.StaticText(panel2, -1, "Upper frequency limit (cpm):")
		self.UpperFrqTxt= wx.TextCtrl(panel2, -1, "%3.2f" % (freqTop), size=(175, -1))
		pwdLabel2 = wx.StaticText(panel2, -1, "Baseline filter type")
		BlkLabel2 = wx.StaticText(panel2, -1, "")
		# self.harshFilter = wx.RadioButton( panel2, -1, " Harsh Filter", style = wx.RB_GROUP )
		# self.mildFilter = wx.RadioButton( panel2, -1, " Mild Filter" )

		sizer = wx.FlexGridSizer(cols=2, hgap=8, vgap=8)

		# sizer.AddMany([basicLabel, self.LowerFrqTxt, pwdLabel, self.UpperFrqTxt, pwdLabel2, BlkLabel2, self.harshFilter, self.mildFilter])
		sizer.AddMany([basicLabel, self.LowerFrqTxt, pwdLabel, self.UpperFrqTxt])
		panel2.SetSizer(sizer)



		cbtn = wx.Button(panel2, label='Change', pos=(120, 110))

		cbtn.Bind(wx.EVT_BUTTON, self.onSendAndClose)

		self.Center()


	#----------------------------------------------------------------------
	def onSendAndClose(self, event):
		"""
		Send a message and close frame
		"""
		# msg = self.msgTxt.GetValue()
		# dispatcher.send("panelListener", message=msg)
		# dispatcher.send("panelListener", message="test2", arg2="2nd argument!")
		# pdb.set_trace()
		# dispatcher.send("panelListener", lowerFrqVal=self.LowerFrqTxt.GetValue(), upperFrqVal=self.UpperFrqTxt.GetValue(), \
		# 				harshFilt=self.harshFilter.GetValue(), mildFilt=self.mildFilter.GetValue())
		dispatcher.send("panelListener", lowerFrqVal=self.LowerFrqTxt.GetValue(), upperFrqVal=self.UpperFrqTxt.GetValue())
		self.Close()

class CheckboxPanel(wx.Frame):

	def __init__(self,conf,Data,pdf_file,summaryRepFlg,lowerFreqLimt,upperFreqLimit,versionNo,harshBfilt):
		"""Constructor"""
		if summaryRepFlg == 0:
			print "detailed report"
			wx.Frame.__init__(self, None, title="Choose channels for analysis (detailed report)",size=(325,250),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
			panel1 = wx.Panel(self)
			txt = wx.StaticText(panel1, label="Choose channels for analysis (detailed report)",pos=(10,10))
		elif summaryRepFlg == 1:
			print "summary rep"
			wx.Frame.__init__(self, None, title="Choose channels for analysis (summary report)",size=(325,250),style= wx.SYSTEM_MENU | wx.CAPTION | wx.CLOSE_BOX)
			panel1 = wx.Panel(self)
			txt = wx.StaticText(panel1, label="Choose channels for analysis (summary report)",pos=(10,10))

		#Button for exit -- maybe to remove?
		button = wx.Button(panel1,label="Exit",pos=(10,180))
		self.Bind(wx.EVT_BUTTON,self.closebutton)
		self.Bind(wx.EVT_CLOSE,self.closewindow)

		self.conf = conf
		self.Data=Data
		self.pdfFile=pdf_file
		self.summaryRepFlg = summaryRepFlg
		self.lowerFreqLimt = lowerFreqLimt
		self.upperFreqLimit = upperFreqLimit
		self.versionNo = versionNo
		self.harshBfilt = harshBfilt

		#Get list of channel types of Data
		A = {}
		B=[]
		for i in Data[0]:
			A[i] = Data[0][i].get('Type')
			# print A[i]
			B.append(A[i])

		#Checkbox
		self.cb_list=[]
		# posList=[(120,75), (120,100), (120,125), (120,150), (120,175), (120,190), (120,210), (120,220)]
		posList=[(40,40), (40,60), (40,80), (40,100), (40,120), (40,140), (40,160), (40,180)]
		for i in range(0,len(B)):
			# cb=wx.CheckBox(panel1,-1,B[i],posList[i])
			cb=wx.CheckBox(panel1,-1,("Channel %d: %s" % (i+1,B[i])),posList[i])
			cb.SetValue(False)
			self.cb_list.append(cb)

		wx.StaticText(panel1, -1, "Send output to File", (70, 255))
		self.cb = wx.CheckBox(panel1, -1, '', (50, 255))
		self.cb.SetValue(True)

		wx.EVT_CHECKBOX(self, self.cb.GetId(), self.ShowTitle)

		self.btnSelect = wx.Button(panel1, label="Select All", pos=(10, 150))
		self.Bind(wx.EVT_BUTTON, self.OnSelectAll, id = self.btnSelect.GetId())

		self.btnUnSelectAll = wx.Button(panel1, label="Unselect All*", pos=(110, 150))
		self.Bind(wx.EVT_BUTTON, self.OnUnSelectAll, id = self.btnUnSelectAll.GetId())

		self.btnApply = wx.Button(panel1, label="Analyze Data", pos=(210, 150))
		self.Bind(wx.EVT_BUTTON, self.OnApply, id = self.btnApply.GetId())

		self.Show()
		self.Centre()


	def closebutton(self,event):
		self.Close(True)

	def closewindow(self,event):
		self.Destroy()

	def ShowTitle(self, event):
		if self.cb.GetValue():
			self.SetTitle('checkbox.py')
		else: self.SetTitle('')

	def OnSelectAll(self, event):
		for cb in self.cb_list:
			cb.SetValue(True)

	def OnUnSelectAll(self, event):
		for cb in self.cb_list:
			cb.SetValue(False)

	def OnApply(self, event):
		self.Hide()
		# msg = "Please wait while we process your request..."
		# busyDlg = wx.BusyInfo(msg)
		selection = self.cb.GetValue()
		for i, cb in enumerate(self.cb_list):
			if cb.GetValue():
				print('{} selected'.format(i))
				if self.Data[0][i].get('Type') == 'CHTYPE_EGG':
					print "Do EGG analysis"
					sigEggProcRep.eggSignalAnalysisReporting(self.conf, self.Data, self.pdfFile,i,self.summaryRepFlg,self.lowerFreqLimt,self.upperFreqLimit,self.versionNo,self.harshBfilt)
				elif self.Data[0][i].get('Type') == 'CHTYPE_ECG':
					print "Do ECG analysis"
					sigEcgProcRep.ecgSignalAnalysisReporting(self.conf, self.Data, self.pdfFile,i,self.summaryRepFlg,self.versionNo)
				elif self.Data[0][i].get('Type') == 'CHTYPE_PRESS':
					print "Do MAP analysis"
					sigPressProcRep.pressureSignalAnalysisReporting(self.conf, self.Data, self.pdfFile,i,self.summaryRepFlg,self.versionNo)
				elif self.Data[0][i].get('Type') == 'CHTYPE_RESP':
					print "Do respiration analysis....no signals present during development"
					wx.MessageBox('Sorry, respiration analysis is not developed', 'Respiration',wx.OK | wx.ICON_INFORMATION)
		# busyDlg = None
		wx.MessageBox('All reports generated', 'Analysis Done',wx.OK | wx.ICON_INFORMATION)
		self.Show()
		print "Done"
		# self.Destroy()

class InputFileFrame(wx.Frame):

	PATIENT_FIELDS = ['Patient', 'PatientID', 'Physician', 'Date']

	def __init__(self, parent, title):
		wx.Frame.__init__(self, parent, title=title, size=(900, 900))

		main_sizer = wx.BoxSizer(wx.HORIZONTAL)

		self.FreqTop = 10
		self.FreqLow = 1
		# self.harshBfilt = True
		# self.mildBfilt = False
		self.versionNo = 20170503

		self.data_config = {}

		main_sizer.Add(self.create_input_panel())
		line = wx.StaticLine(self, wx.ID_ANY, size=(4, 4), style=wx.LI_VERTICAL)
		#line.SetSize((30,30))
		main_sizer.Add(line, flag=wx.EXPAND)
		main_sizer.Add(self.create_patient_panel())

		#self.create_patient_panel()

		self.SetSizer(main_sizer)
		self.Layout()
		self.SetMinSize(main_sizer.GetMinSize())
		self.Fit()

		self.Bind(wx.EVT_CLOSE, self.closeWindow)


	def closeWindow(self,event):
		self.Destroy()

	def create_input_panel(self):

		self.input_panel = wx.Panel(self, wx.ID_ANY)

		#main_sizer = wx.BoxSizer(wx.VERTICAL)
		#first_file_sizer = wx.BoxSizer(wx.HORIZONTAL)
		#second_file_sizer = wx.BoxSizer(wx.HORIZONTAL)
		#third_file_sizer = wx.BoxSizer(wx.HORIZONTAL)
		#fourth_file_sizer = wx.BoxSizer(wx.HORIZONTAL)
		#main_sizer = wx.GridSizer(rows=3, cols=3, hgap=5, vgap=5)
		input_sizer = wx.GridBagSizer(hgap=7, vgap=3)
		input_sizer.AddGrowableCol(1)

		#title =
		input_sizer.Add(wx.StaticText(self.input_panel, wx.ID_ANY, 'Electrogastrogram Analysis Package'), pos=(0, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.ALIGN_CENTER_HORIZONTAL, border=5)
		#first_file_sizer.Add(wx.StaticText(self.panel, wx.ID_ANY, 'S01 File'))
		input_sizer.Add(wx.StaticText(self.input_panel, wx.ID_ANY, 'S01 File'), pos=(1, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
		self.s01 = input_1 = wx.TextCtrl(self.input_panel, wx.ID_ANY, '')
		width, _ = input_1.GetTextExtent('blah' * 15)
		_, height = input_1.GetSize()
		input_1.SetInitialSize((width, height))
		#first_file_sizer.Add(input_1, wx.EXPAND | wx.TOP | wx.BOTTOM)
		input_sizer.Add(input_1, pos=(1, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)
		first_file_button = wx.Button(self.input_panel, wx.ID_ANY, 'Choose')
		first_file_button.name = 's01'
		first_file_button.Bind(wx.EVT_BUTTON, self.choose_file)
		#first_file_sizer.Add(first_file_button)
		input_sizer.Add(first_file_button, pos=(1, 2), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		#second_file_sizer.Add(wx.StaticText(self.panel, wx.ID_ANY, 'C00 File'))
		input_sizer.Add(wx.StaticText(self.input_panel, wx.ID_ANY, 'C00 File'), pos=(2, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
		self.c00 = input_2 = wx.TextCtrl(self.input_panel, wx.ID_ANY, '')
		#second_file_sizer.Add(input_2, wx.EXPAND)
		input_sizer.Add(input_2, pos=(2, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)
		second_file_button = wx.Button(self.input_panel, wx.ID_ANY, 'Change')
		second_file_button.name = 'c00'
		second_file_button.Bind(wx.EVT_BUTTON, self.choose_file)
		#second_file_sizer.Add(second_file_button)
		input_sizer.Add(second_file_button, pos=(2, 2), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		#third_file_sizer.Add(wx.StaticText(self.panel, wx.ID_ANY, 'C01 File'))
		input_sizer.Add(wx.StaticText(self.input_panel, wx.ID_ANY, 'C01 File'), pos=(3, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
		self.c01 = input_3 = wx.TextCtrl(self.input_panel, wx.ID_ANY, '')
		#third_file_sizer.Add(input_3, wx.EXPAND)
		input_sizer.Add(input_3, pos=(3, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)
		third_file_button = wx.Button(self.input_panel, wx.ID_ANY, 'Change')
		third_file_button.name = 'c01'
		third_file_button.Bind(wx.EVT_BUTTON, self.choose_file)
		#third_file_sizer.Add(third_file_button)
		input_sizer.Add(third_file_button, pos=(3, 2), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		#fourth_file_sizer.Add(wx.StaticText(self.panel, wx.ID_ANY, 'output_pdf'))
		input_sizer.Add(wx.StaticText(self.input_panel, wx.ID_ANY, 'Output pdf'), pos=(4, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
		self.pdf = input_4 = wx.TextCtrl(self.input_panel, wx.ID_ANY, '')
		#fourth_file_sizer.Add(input_4, wx.EXPAND)
		input_sizer.Add(input_4, pos=(4, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)
		fourth_file_button = wx.Button(self.input_panel, wx.ID_ANY, 'Change')
		fourth_file_button.name = 'pdf'
		fourth_file_button.Bind(wx.EVT_BUTTON, self.choose_file)
		#fourth_file_sizer.Add(fourth_file_button)

		#5_sizer
		# self.changePara_button = changePara_button = wx.Button(self.input_panel, wx.ID_ANY, 'Change parameters')
		# input_sizer.Add(changePara_button, pos=(5, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		self.acem_button = acem_button = wx.Button(self.input_panel, wx.ID_ANY, 'Generate MS Excel File')
		acem_button.Bind(wx.EVT_BUTTON, partial(self.acemReport,summaryRepFlg=2))
		input_sizer.Add(acem_button , pos=(7, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL  | wx.EXPAND, border=5)

		dispatcher.connect(self.myListener, signal="panelListener",sender=dispatcher.Any)

		self.changePara_button = changePara_button = wx.Button(self.input_panel, wx.ID_ANY, 'Modify \n Parameters')
		changePara_button.SetBackgroundColour('Light Gray')
		changePara_button.Bind(wx.EVT_BUTTON, self.changeParameters)
		input_sizer.Add(changePara_button , pos=(5, 2), span=(2,1) , flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL  | wx.EXPAND, border=5)

		#6 sizer
		self.summaryRep_button = summaryRep_button  = wx.Button(self.input_panel, wx.ID_ANY, 'Generate Summary Report')
		summaryRep_button.Bind(wx.EVT_BUTTON, partial(self.go,summaryRepFlg=1))
		input_sizer.Add(summaryRep_button  , pos=(5, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL  | wx.EXPAND, border=5)

		#7 sizer
		self.go_button = go_button = wx.Button(self.input_panel, wx.ID_ANY, 'Generate Detailed Report')
		# go_button.Bind(wx.EVT_BUTTON, self.go)
		go_button.Bind(wx.EVT_BUTTON, partial(self.go,summaryRepFlg=0))
		input_sizer.Add(go_button, pos=(6, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)

		self.exit_button = exit_button = wx.Button(self.input_panel, wx.ID_ANY, 'Exit')
		exit_button.Bind(wx.EVT_BUTTON, self.closeWindow)
		input_sizer.Add(exit_button, pos=(7, 2), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL | wx.EXPAND, border=5)

		input_sizer.Add(fourth_file_button, pos=(4, 2), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		self.about_button = about_button= wx.Button(self.input_panel, wx.ID_ANY, 'About')
		about_button.Bind(wx.EVT_BUTTON, self.aboutFunc)
		input_sizer.Add(about_button , pos=(7, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL  | wx.EXPAND, border=5)

		#main_sizer.Add(title, 0, wx.CENTER, 5)
		#main_sizer.Add(wx.StaticLine(self.panel), 0, wx.ALL | wx.EXPAND, 5)
		#main_sizer.Add(first_file_sizer, 0, wx.ALL | wx.EXPAND, 5)
		#main_sizer.Add(second_file_sizer, 0, wx.ALL | wx.EXPAND, 5)
		#main_sizer.Add(third_file_sizer, 0, wx.ALL | wx.EXPAND, 5)
		#main_sizer.Add(fourth_file_sizer, 0, wx.ALL | wx.EXPAND, 5)



		self.input_panel.SetSizer(input_sizer)
		#input_sizer.Fit(self)

		return self.input_panel

	def create_patient_panel(self):
		patient_panel = wx.Panel(self, wx.ID_ANY)
		patient_sizer = wx.GridBagSizer(hgap=5, vgap=5)
		patient_sizer.AddGrowableCol(1)

		patient_sizer.Add(wx.StaticText(patient_panel, wx.ID_ANY, 'Patient Info'), pos=(0, 0), span=(1, 2), flag=wx.ALL | wx.ALIGN_CENTER, border=5)

		self.patient_labels = {}
		for i, field in enumerate(InputFileFrame.PATIENT_FIELDS):
			patient_sizer.Add(wx.StaticText(patient_panel, wx.ID_ANY, field), pos=(i + 1, 0), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)
			self.patient_labels[field] = wx.StaticText(patient_panel, wx.ID_ANY, 'None')
			patient_sizer.Add(self.patient_labels[field], pos=(i + 1, 1), flag=wx.ALL | wx.ALIGN_CENTER_VERTICAL, border=5)

		width, _ = self.patient_labels.values()[0].GetTextExtent('blah' * 5)
		_, height = self.patient_labels.values()[0].GetSize()
		self.patient_labels.values()[0].SetInitialSize((width, height))

		patient_panel.SetSizer(patient_sizer)
		#patient_sizer.Fit(self)

		return patient_panel

	def populate_patient_labels(self):
		conf = self.data_config['Patient']
		for key, label in self.patient_labels.iteritems():
			if key not in conf:
				label.SetLabel('Missing')
			else:
				label.SetLabel(conf[key])

	def go(self, evt, summaryRepFlg):
		print 'it\'s time to go'
		# msg = "Please wait while we process your request..."
		# busyDlg1 = wx.BusyInfo(msg)
		c00_file = self.c00.Value
		c01_file = self.c01.Value
		s01_file = self.s01.Value
		pdf_file = self.pdf.Value

		print "Upper frequency limit %3.2f" % float(self.FreqTop)
		print "Lower frequency limit %3.2f" % float(self.FreqLow)
		#Frequency parameters
		lowerFreqLimt = float(self.FreqLow)
		upperFreqLimit = float(self.FreqTop)
		harshFilter = self.harshBfilt
		mildFilter = self.mildBfilt

		print '{}\n+  {}\n+  {}\n-> {}'.format(c00_file, c01_file, s01_file, pdf_file)

		try:
			readconfigfilename = self.c01.Value.strip()
			if not(readconfigfilename):
				readconfigfilename = self.c00.Value.strip()

			print sandhill_input.get_data(readconfigfilename, s01_file)
			Data = sandhill_input.get_data(readconfigfilename, s01_file)
			conf = self.data_config['Patient']

			frameChk = CheckboxPanel(conf,Data,pdf_file,summaryRepFlg,lowerFreqLimt,upperFreqLimit,self.versionNo,self.harshBfilt)
			frameChk.Show()
			self.Show()
		except:
			print "Error - config or data files not being read properly"
			pass



	def choose_file(self, evt):
		dlg = wx.FileDialog(
			self, message="Choose a file",
			defaultDir=os.path.curdir,
			defaultFile="",
			style=wx.OPEN | wx.MULTIPLE | wx.CHANGE_DIR
			)
		if dlg.ShowModal() == wx.ID_OK:
			paths = dlg.GetPaths()
			#print paths
			#file = ''.join((str(p) for p in paths))
			file = paths[0]
			#self.file_indicator.SetValue(file)
			#print file[:-4] + ''
			#print

			if evt.GetEventObject().name == 's01':
				self.s01.SetValue(file)
				if os.path.exists(file[:-4] + '.c00'):
					self.c00.SetValue(file[:-4] + '.c00')
				else:
					self.c00.SetValue('')
				if os.path.exists(file[:-4] + '.c01'):
					self.c01.SetValue(file[:-4] + '.c01')
				else:
					self.c01.SetValue('')
				self.pdf.SetValue(file[:-4] + '.pdf')
			elif evt.GetEventObject().name == 'c00':
				self.c00.SetValue(file)
			elif evt.GetEventObject().name == 'c01':
				self.c01.SetValue(file)
			elif evt.GetEventObject().name == 'pdf':
				self.pdf.SetValue(file)

		try:
			readconfigfilename = self.c01.Value.strip()
			if not(readconfigfilename):
				readconfigfilename = self.c00.Value.strip()
			self.data_config = sandhill_input.get_config(readconfigfilename)
			self.populate_patient_labels()
		except:
			print "Error - Data config not extracted"
			pass

	def acemReport(self,evt,summaryRepFlg):
		print "ACEM REPORTING"


		print 'it\'s time to generate ACEM report'
		lowerFreqLimt = float(self.FreqLow)
		upperFreqLimit = float(self.FreqTop)
		c00_file = self.c00.Value
		c01_file = self.c01.Value
		s01_file = self.s01.Value
		pdf_file = self.pdf.Value
		print '{}\n+  {}\n+  {}\n-> {}'.format(c00_file, c01_file, s01_file, pdf_file)

		try:
			configFile = c01_file
			if not configFile:
				configFile = c00_file
			print sandhill_input.get_data(configFile, s01_file)
			Data = sandhill_input.get_data(configFile, s01_file)
			conf = self.data_config['Patient']

			acemSig.signalProcAnalysis(conf, Data, pdf_file,summaryRepFlg,configFile,lowerFreqLimt,upperFreqLimit,self.versionNo, self.harshBfilt)

		except:
			print "Error - config or data files not being read properly"
			pass


	def changeParameters(self,evt):
		print "TO DO - Change Parameters"
		frame1 = OtherFrame()
		frame1.Show()


	def myListener(self, lowerFrqVal, upperFrqVal):
		"""
		Listener function
		"""
		print "Received the following message: " + lowerFrqVal
		# if upperFrqVal:
		# 	print "Received another arguments: " + str(upperFrqVal)
		self.FreqTop = upperFrqVal
		self.FreqLow = lowerFrqVal
		# self.harshBfilt = harshFilt
		# self.mildBfilt = mildFilt
		# pdb.set_trace()


	def aboutFunc(self,evt):
		print "TO DO - About Function"

		info = wx.AboutDialogInfo()
		info.Name = "Electrogastrogram Analysis Package"
		# info.Version = "10122014 Beta"
		info.Version = "(" + str(self.versionNo) + ")"
		info.Copyright = "(C) 2014 Auckland Bioengineering Institute, University of Kentucky, Kentucky One Health"
		info.Description = textwrap.fill("This software program was developed to analyze gastrointestinal "
										 "slow wave activity as recorded from the Sandhill recording "
										 "system. Report and/or excel files are generated with quantitative metrics of interest."
										 " This program is currently used for research, development and"
										 " potentially clinical use.", width = 55)
		info.Developers = [textwrap.fill("Dr. Niranchan Paskaranandavadivel, Mr. Simon Bull,          "
										 "Assoc. Prof. Leo K Cheng, Dr. Douglas Parsell, Prof. Thomas Abell",width=65)]
		info.Licence =textwrap.fill("Proprietary to Prof. Thomas Abell, Kentucky One Health")
		# Show the wx.AboutBox
		wx.AboutBox(info)


app = wx.App(False)  # Create a new app, don't redirect stdout/stderr to a window.
frame = InputFileFrame(None, "Electrogastrogram Analysis Package")
frame.Center()
frame.Show(True)     # Show the frame.
app.MainLoop()
