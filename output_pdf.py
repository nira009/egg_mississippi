from matplotlib.backends.backend_pdf import PdfPages

def savePdf(A,output_filename):

	# Inputs: A - list of object figures
	#	  output_filename - filename for the pdf
	# Output: pdf file save in prescribed location in output_filename 
	#________________________#
	#    PUTTING INTO PDF    #
	#~~~~~~~~~~~~~~~~~~~~~~~~#

	# Pdfsave_name = output_filename+'_output.pdf'

	#Save into pdf page
	pp = PdfPages(output_filename)

	#Loop through all of the figures present
	for i, figure in enumerate(A):
		pp.savefig(A[i],rotate=90,bbox_inches='tight')

	#Close the object
	pp.close()



if __name__ == '__main__':

	import matplotlib.pyplot as plt

	#Dummy start page
	fig_start = plt.figure(figsize=[10,10])	
	txt = '''First name : Testing, \nLast name  : Testing,\nPhysician  :Testing,\nTime of reading : Testing,\n**Testing of Automatic Report Generation**'''
	fig_start.text(.3,.7,txt,fontsize=15)


	#Dummy figures of plots
	fig_far = plt.figure(figsize=(10,10))
	plt.plot([1,2,3,4,5,6,7,8,9,10])

	fig_sig = plt.figure(figsize=(10,10))
	plt.plot([10,9,8,7,6,5,4,3,2,1,0])

	#Figures in list
	A = [fig_start,fig_far,fig_sig]
	output_filename='Test'
	savePdf(A,output_filename)
	
