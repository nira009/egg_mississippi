'''
Created on 17/02/2014

@author: npas004
'''

import pylab as pylab


import numpy as np
import matplotlib.pyplot as plt 
import matplotlib.gridspec as gridspec
import time
import pdb


def start(data,t,ampl,maxfr,filename):
    

    #initialsie lists
    start_time=[]
    dataAmpl = []
    dataFrq = []
    
    nV=0

    for i in range(0,len(data)):
        
        if (i ==0):
            
            if (data[i] == 1):# if start
                start_time.append([])
                start_time[nV].append(t[i])
                
                dataAmpl.append([])
                dataAmpl[nV].append(ampl[i])
                
                dataFrq.append([])
                dataFrq[nV].append(maxfr[i])


        else:
                
            if (data[i] == 1) and (data[i-1] != 1) : #initialise
                
                if (nV==0) and not start_time: #initialise if very first
                    start_time.append([])
                    start_time[nV].append(t[i])

                    dataAmpl.append([])
                    dataAmpl[nV].append(ampl[i])
                    
                    dataFrq.append([])
                    dataFrq[nV].append(maxfr[i])
                    
                else:

                    nV=nV+1
                    start_time.append([])
                    start_time[nV].append(t[i])    

                    dataAmpl.append([])
                    dataAmpl[nV].append(ampl[i])
                    
                    dataFrq.append([])
                    dataFrq[nV].append(maxfr[i])                   
                                  
                print nV


            elif (data[i] == 1) and (data[i-1] == 1) : #normal append
                start_time[nV].append(t[i])
                dataAmpl[nV].append(ampl[i])
                dataFrq[nV].append(maxfr[i])
                
                
#        print "i is "+str(i)
#        print nV
#        print start_time

                

    #Writing values to text file
    print "Printing out results"
    print filename[:-4]+'.txt'
    text_file = open(filename[:-4]+'_goodSegment.txt', "w")
    text_file.write("Start time (min) \t End time (min) \t Avg frequency(cpm)"
                        " \t Avg amplitude (mV) \n")

    for i in range(len(start_time)-1):
        print i

#        print "%3.2f \t %3.2f \t %3.2f \t %3.2f" % (start_time[i][0],start_time[i][-1],np.mean(dataFrq[i]),np.mean(dataAmpl[i]))
        text_file.write("%3.2f \t %3.2f \t %3.2f \t %3.2f \n" % (start_time[i][0]/60,start_time[i][-1]/60,np.mean(dataFrq[i])*60,np.mean(dataAmpl[i])))

    
    text_file.close()
    
    print "done writing to file"
    
        
        

        
if __name__ == '__main__':
    #Testing if code works
    A=np.array([0, 0, 1, 1, 1, 1, 1, 1, 0 , 0, 0,1,1,1,1,1,0,0,0,1,1,1,1,1,1,0,0,0,0,1,1,1,0,1,1,1,0])
    start(A,A,A,A,'asdfdsfssd')    
    

