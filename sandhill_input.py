from pyparsing import (Suppress, quotedString, removeQuotes, Word, alphas, nums, Group, restOfLine, Combine, Forward, Dict, ZeroOrMore)

import struct
import numpy as np

from optparse import OptionParser

_config_definition = None

def read_conf(f):
	global _config_definition
	if _config_definition is None:
		EQ = Suppress('=')
		SEMI = Suppress(';')
		BEGIN = Suppress('Begin')
		END = Suppress('End')

		quotedString.setParseAction(removeQuotes)

		ident = quotedString | Word(alphas + nums)
		number = Word(nums).setParseAction(lambda s, l, t: [int(t[0])])

		defn = Group(ident + EQ + quotedString) | Group(ident + EQ + number + restOfLine) | Group(ident + EQ + restOfLine)
		
		section_head = Suppress('Begin') + Combine(ident + SEMI + number, adjacent=False) | BEGIN + ident
		section_foot = Suppress(END + ident + SEMI + number | END + ident)

		section = Forward()
		section << Group(section_head + Dict(ZeroOrMore(defn | section)) + section_foot)

		_config_definition = Dict(ZeroOrMore(defn)) + Dict(ZeroOrMore(section))
	


	result = _config_definition.parseString(f.read())

	return result

def get_channel_info(config):
	channels = {}
	conf = config.asDict()
	required_items = [
			'Type',
			'CalibrationMeasuredOffset',
			'CalibrationDeltaPhysical',
			'CalibrationDeltaMeasured',
			'CalibrationPhysicalOffset'
			]
	for k in conf:
		if k.startswith('Procedure'):
			proc = conf[k].asDict()
			#print k
			#print ' SampleRate', proc['SampleRate'][0]
			for key in proc:
				if key.startswith('Channel'):
					chan_index = int(key[7:]) - 1
					channels[chan_index] = {}
					for item in required_items:
						if isinstance(proc[key][item], basestring):
							channels[chan_index][item] = proc[key][item].strip()
						else:
							channels[chan_index][item] = proc[key][item][0]
	return channels

def read_data(chan_info, f):
	raw = f.read()
	num_chans = len(chan_info) 
	data = struct.unpack('H' * (len(raw) / 2), raw)
	data = np.array(data, dtype=np.float32)
	
	data = data.reshape((len(data) / num_chans, num_chans))
	for key in sorted(chan_info.iterkeys()):
		info = chan_info[key]
		data[:, key] -= info['CalibrationMeasuredOffset']
		data[:, key] *= 1.0 * info['CalibrationDeltaPhysical'] / info['CalibrationDeltaMeasured']
		data[:, key] += info['CalibrationPhysicalOffset']

	return data

def get_config(config_filename):
	with open(config_filename, 'rb') as f:
		conf = read_conf(f)
	return conf

def get_data(config_filename, data_filename):
	with open(config_filename, 'rb') as f:
			conf = read_conf(f)
	chan_info = get_channel_info(conf)
	with open(data_filename, 'rb') as f:
		data = read_data(chan_info, f)
	return chan_info, data

if __name__ == '__main__':
	import sys
	parser = OptionParser()
	parser.add_option('-c', '--config', help='Config file (c00, c02)')
	parser.add_option('-d', '--data', help='Data file (S00, S01)')
	parser.add_option('-o', '--output', help='Output file (csv file)')

	(options, args) = parser.parse_args()
	if options.config is not None:
		with open(options.config, 'rb') as f:
			conf = read_conf(f)
		chan_info = get_channel_info(conf)
	else:
		parser.print_help()
		sys.exit()

	print len(chan_info), 'channels found in config file.'

	if options.data is not None:
		with open(options.data, 'rb') as f:
			data = read_data(chan_info, f)
		l, n = np.shape(data)
		print l, 'data records found'

	if options.output is not None:
		with open(options.output, 'wb') as f:
			for row in data:
				f.write(','.join(str(num) for num in row) + '\n')
