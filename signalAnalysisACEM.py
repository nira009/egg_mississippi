__author__ = 'npas004'


import pdb as pdb
import signal_egg
import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
import output_pdf as output
import wx as wx
from datetime import timedelta
from pylab import *
import findIndices as findIndices
import math as math
import matplotlib.gridspec as gridspec

import processGoodSegmentAverage as processRep

import EGG_signalAnalysisReporting as sigEggProcRep
import ECG_signalAnalysisReporting as sigEcgProcRep
import PRESSURE_signalAnalysisReporting as sigPressProcRep
import acquireEvents as acquireEvents
import xlsxwriter
import collections
import scipy.io as sio

def eventDifference(events,Data):
	print "finding events"
	# EventInfo = collections.OrderedDict()
	EventInfo = []
	EventInfoTime = []
	EventInfoTimeHMS=[]

	for eventType in events:
		print eventType

		if eventType[0][0:5] == "Event":
			# EventInfo[eventType[0]]=eventType[1][1] #name of event
			# pdb.set_trace()
			if eventType[1][1] == "$EPD": #restart list if EPD as it is start of recording file
					EventInfo = []
					EventInfoTime = []
					EventInfoTimeHMS = []
			for jj in eventType:
				# if jj[0] == "StripchartFileIndex":
				if jj[0].lower() == "stripchartfileindex": #Stripchart index unreliable due to multiple recordings
					timeStpChrt = jj[1]/300.0 #time in seconds
				if jj[0].lower() == "time": #save time as well - EventInfoTimeHMS.append(jj[1])
					origTime = jj[1]

			if  (eventType[1][1] == "$EPD") and ( timeStpChrt> np.float(1/30.0)):
				# pdb.set_trace()
				EventInfo = []
				EventInfoTime = []
				EventInfoTimeHMS=[]
				timeStpChrt=0
				# print EventInfo
				# EventInfo.append('Starting0')
				# EventInfoTime.append(0)
				# EventInfoTimeHMS.append('StartFile')

			# EventInfoTime.append(jj[1]/300.0)
			EventInfo.append(eventType[1][1])
			EventInfoTime.append(timeStpChrt)
			EventInfoTimeHMS.append(origTime)
			print EventInfo
			# pdb.set_trace()


	if max(EventInfoTime) < ((len(Data[1])/30.0)-1):
		EventInfo.append('Ending0')
		EventInfoTime.append(len(Data[1])/30.0)
		EventInfoTimeHMS.append('EndOfFile')
	# pdb.set_trace()

	#6April2016 - Retesting the time differnce based on displayed time
	FMT = '%H:%M:%S'
	# EventInfoTime1=[]
	# EventInfoTime1.append(0)
	for idx, val in enumerate(EventInfoTimeHMS[0:-1]):
		if idx > 0:
			tdelta = datetime.datetime.strptime(EventInfoTimeHMS[idx], FMT) - datetime.datetime.strptime(EventInfoTimeHMS[idx-1], FMT)
			print tdelta.seconds
			EventInfoTime[idx]=(tdelta.seconds) + EventInfoTime[idx-1]
		print (idx, val)

	if EventInfo[-1] =='Ending0':
		lastTime = datetime.datetime.strptime(EventInfoTimeHMS[-2], FMT) + datetime.timedelta(seconds = EventInfoTime[-1]-EventInfoTime[-2])
		EventInfoTimeHMS[-1] =lastTime.strftime(FMT)
	print "done success"
	# pdb.set_trace()




	return EventInfo, EventInfoTime, EventInfoTimeHMS

def find_nearestInd(array, t1,t2):
	'''
	Helper function to find the nearest value in an array
	'''
	idx1=0
	idx2=0
	for i in range(0,len(array)):
		if array[i] <t1:
			idx1=i
		if array[i]<t2:
			idx2=i
	return idx1,idx2




def signalProcAnalysis (patient_info, Data, output_file,summaryFlag,configFile,lowerFreqLimit, upperFreqLimit,versionNo,harshFilter):
	'''
	Processing of the input data
	'''

	events = acquireEvents.getEvents(configFile)
	# pdb.set_trace()
	[EventInfo,EvtTime,EventInfoTimeHMS] = eventDifference(events,Data)
	stdDevNo = 3
	A = {}
	B=[]
	DictOverall=collections.OrderedDict()
	AvgResultsTimeSegment = collections.OrderedDict()
	StdResultsTimeSegment = collections.OrderedDict()

	egg_restingState = []
	egg_standingState=[]
	egg_ColdStimState = []

	#MAIN LOOP to go over all the signals and perform apporiate analysis
	for i in Data[0]:
		A[i] = Data[0][i].get('Type')
		B.append(A[i])
		if A[i] == 'CHTYPE_EGG':
			print "EGG signal"
			[t,ampl,maxfr,frqPeaksOneTen,validPltPoint] = sigEggProcRep.eggSignalAnalysisReporting(patient_info, Data, output_file,i,summaryFlag,lowerFreqLimit,upperFreqLimit,versionNo,harshFilter)
			DictOverall['Time_(s)_EGG_Ch{}'.format(i+1)] = t
			DictOverall['Ampl_(mV)_EGG_Ch{}'.format(i+1)] = np.asarray(ampl)
			DictOverall['MaxFr_(cpm)_EGG_Ch{}'.format(i+1)] = np.asarray(maxfr)
			validPltPoint[isnan(validPltPoint)]=0
			DictOverall['ValidPt_EGG_Ch{}'.format(i+1)] = np.asarray(validPltPoint)

			print "egg to calc estimates"

			print "to calc estimates of data inbetween"
			eggMeanFrqResults=[]
			eggMeanAmplResults=[]
			eggStdFrqResults=[]
			eggStdAmplResults=[]

			for ii in range(0,len(EvtTime)-1):
				[idx1,idx2] = find_nearestInd(t, EvtTime[ii],EvtTime[ii+1])
				dummyChkValFrq=[]
				dummyChkValAmpl=[]
				for j in range(idx1,idx2+1):
					if validPltPoint[j] ==1:
						dummyChkValFrq.append(maxfr[j])
						dummyChkValAmpl.append(ampl[j])
				# print 'EVENT{}'.format(ii)
				# pdb.set_trace()
				if len(dummyChkValFrq)<2:
					dummyChkValFrq=np.nan
					dummyChkValAmpl=np.nan
					tst3std1=1;tst3std2=1;tst3std3=1;tst3std4 =1;
				else:
					tst3std1 = np.max(dummyChkValFrq) > (np.mean(dummyChkValFrq)+np.std(dummyChkValFrq)*stdDevNo)
					tst3std2 = np.min(dummyChkValFrq) < (np.mean(dummyChkValFrq)-np.std(dummyChkValFrq)*stdDevNo)
					tst3std3 = np.max(dummyChkValAmpl) > (np.mean(dummyChkValAmpl)+np.std(dummyChkValAmpl)*stdDevNo)
					tst3std4 = np.min(dummyChkValAmpl) < (np.mean(dummyChkValAmpl)-np.std(dummyChkValAmpl)*stdDevNo)
				eggMeanFrqResults.append(np.mean(dummyChkValFrq))
				eggMeanAmplResults.append(np.mean(dummyChkValAmpl))
				if tst3std1 or tst3std2 or tst3std3 or tst3std4:
					# pdb.set_trace()
					eggStdFrqResults.append(0)
					eggStdAmplResults.append(0)
				else:
					eggStdFrqResults.append(1)
					eggStdAmplResults.append(1)

				if EventInfo[ii] == 'sitting' or EventInfo[ii] == 'sit': # for ACEM
					egg_restingState.append(np.mean(dummyChkValFrq))
				if EventInfo[ii] == 'stand' or EventInfo[ii] == 'standing': # for ACEM
					egg_standingState.append(np.mean(dummyChkValFrq))
				if EventInfo[ii] == 'cold stim' or  EventInfo[ii] == 'cold': # for ACEM
					egg_ColdStimState.append(np.mean(dummyChkValFrq))


				# eggStdFrqResults.append(np.std(dummyChkValFrq))
				# eggStdAmplResults.append(np.std(dummyChkValAmpl))
			print 'EGG_FRQ_(cpm)_Ch{}'.format(i+1)
			AvgResultsTimeSegment['EGG_FRQ_(cpm)_Ch{}'.format(i+1)] = np.asarray(eggMeanFrqResults)
			AvgResultsTimeSegment['EGG_Ampl_(mv)_Ch{}'.format(i+1)] = np.asarray(eggMeanAmplResults)
			StdResultsTimeSegment['EGG_FRQ_(cpm)_Ch{}'.format(i+1)] = np.asarray(eggStdFrqResults)
			StdResultsTimeSegment['EGG_Ampl_(mv)_Ch{}'.format(i+1)] = np.asarray(eggStdAmplResults)


		elif A[i] == 'CHTYPE_RESP':
			print "RESPIRATION"


		elif A[i] == 'CHTYPE_ECG':
			print "ECG SIGNAL"
			# pdb.set_trace()
			[t,maxFrq,validPt]= sigEcgProcRep.ecgSignalAnalysisReporting(patient_info, Data, output_file,i,summaryFlag,versionNo)
			maxFrq=maxFrq*60 #convert to bpm
			# plt.figure(),plt.plot(t,maxFrq),plt.show()
			# pdb.set_trace()
			DictOverall['Time_ECG_(s)_Ch{}'.format(i+1)] = t
			DictOverall['MaxFr_(bpm)_ECG_Ch{}'.format(i+1)] = np.asarray(maxFrq)
			DictOverall['ValidPt_ECG_Ch{}'.format(i+1)] = np.asarray(validPt)

			print "ecg to calc estimates"
			print "to calc estimates of data inbetween"
			hrMeanResults=[]
			hrStdResults=[]

			for ii in range(0,len(EvtTime)-1):
				# print ii
				dummyChkVal=[]
				[idx1,idx2] = find_nearestInd(t, EvtTime[ii],EvtTime[ii+1])
				for j in range(idx1,idx2+1):
					# print j
					if validPt[j] ==1:
						dummyChkVal.append(maxFrq[j])
				if len(dummyChkVal)<2:
					# dummyChkVal=np.nan
					dummyChkVal.append(np.nan)
					test3std1 =1;test3std2 =1
				else:
					test3std1 = np.max(dummyChkVal) > (np.mean(dummyChkVal)+np.std(dummyChkVal)*stdDevNo)
					test3std2 = np.min(dummyChkVal) < (np.mean(dummyChkVal)-np.std(dummyChkVal)*stdDevNo)
				# hrMeanResults.append(np.mean(dummyChkVal))
				hrMeanResults.append(np.mean(dummyChkVal))
				if test3std1 or test3std2:
					hrStdResults.append(0)
				else:
					hrStdResults.append(1)

				if EventInfo[ii] == 'deep breathing': # for ACEM
					# pdb.set_trace()
					changeHR_breating = np.min(dummyChkVal)
				if EventInfo[ii] == 'stand' or EventInfo[ii] == 'standing': # for ACEM
					print "standing"
					# pdb.set_trace()
					HR_standing_first = dummyChkVal[0]
				if EventInfo[ii] == 'sitting' or EventInfo[ii] == 'sit': # for ACEM
					print "sitting"
					# pdb.set_trace()
					HR_sitting_last = dummyChkVal[-1]

				# hrStdResults.append(np.std(dummyChkVal))
			# pdb.set_trace()
			AvgResultsTimeSegment['ECG_HR_(bpm)_Ch{}'.format(i+1)] = np.asarray(hrMeanResults)
			StdResultsTimeSegment['ECG_HR_(bpm)_Ch{}'.format(i+1)] = np.asarray(hrStdResults)

			allHRdata =[] # for ACEM
			for x, y in zip(maxFrq, validPt):
				if y ==1:
					allHRdata.append(x)
			maxHeartRate = np.max(np.asarray(allHRdata))
			minHeartRate = np.min(np.asarray(allHRdata))
			# pdb.set_trace()


		elif A[i] == 'CHTYPE_PRESS':
			print "PRESSURE"
			[t,maPres,maFreq,validPtTPA] = sigPressProcRep.pressureSignalAnalysisReporting(patient_info, Data, output_file,i,summaryFlag,versionNo)
			# plt.figure(),plt.plot(t,maPres),plt.title('Pressure'),plt.show()
			# plt.figure(),plt.plot(t,maFreq),plt.title('Frequency'),plt.show()
			# pdb.set_trace()
			maFreq = maFreq * 60.0 #convert to beats per minute
			# plt.figure(),plt.plot(t,maFreq),plt.show()
			TPA_all = maFreq * maPres # total pulse amplitude
			# plt.figure(),plt.plot(t,TPA_all),plt.show()
			DictOverall['Time_MAP_(s)_Ch{}'.format(i+1)] = t
			DictOverall['Pressure_MAP_(mmHg)_Ch{}'.format(i+1)] = np.asarray(maPres)
			DictOverall['Pressure_Frq_(bpm)_Ch{}'.format(i+1)] = np.asarray(maFreq)

			print "bp to calc estimates of data inbetween"
			mapMeanResults=[]
			mapStdResults=[]
			mapFrqMeanResults=[]
			mapFrqStdResults=[]
			for ii in range(0,len(EvtTime)-1):
				# print ii
				# pdb.set_trace()
				[idx1,idx2] = find_nearestInd(t, EvtTime[ii],EvtTime[ii+1])
				mapMeanResults.append(np.mean(maPres[idx1:idx2+1]))
				mapFrqMeanResults.append(np.mean(maFreq[idx1:idx2+1]))

				testy3std1 = np.max(maPres[idx1:idx2+1]) > (np.mean(maPres[idx1:idx2+1])+np.std(maPres[idx1:idx2+1])*stdDevNo)
				testy3std2 = np.min(maPres[idx1:idx2+1]) < (np.mean(maPres[idx1:idx2+1])-np.std(maPres[idx1:idx2+1])*stdDevNo)

				if testy3std1 or testy3std2:
					mapStdResults.append(0)
				else:
					mapStdResults.append(1)

				testy33std1 = np.max(maFreq[idx1:idx2+1]) > (np.mean(maFreq[idx1:idx2+1])+np.std(maFreq[idx1:idx2+1])*stdDevNo)
				testy33std2 = np.min(maFreq[idx1:idx2+1]) < (np.mean(maFreq[idx1:idx2+1])-np.std(maFreq[idx1:idx2+1])*stdDevNo)
				if testy33std1 or testy33std2:
					mapFrqStdResults.append(0)
				else:
					mapFrqStdResults.append(1)

				if EventInfo[ii] == 'stand' or EventInfo[ii] == 'standing': # for ACEM
					dummyChkVal=[]
					for j in range(idx1,idx2+1):
						if validPtTPA[j] ==1:
							dummyChkVal.append(TPA_all[j])
					if not dummyChkVal:
						maxTPA_stand =0
						minTPA_stand =0
					else:
						pdb.set_trace()
						maxTPA_stand = np.max(dummyChkVal)
						minTPA_stand = np.min(dummyChkVal)

				if EventInfo[ii] == 'cold stim' or EventInfo[ii] == 'cold': # for ACEM
					dummyChkVal=[]
					for j in range(idx1,idx2+1):
						if validPtTPA[j] ==1:
							dummyChkVal.append(TPA_all[j])
					if not dummyChkVal:#check if any Data exists or else set to 0
						meanTPA_coldStim =0
					else:
						meanTPA_coldStim = np.mean(dummyChkVal)
					# pdb.set_trace()

				if EventInfo[ii] == 'sitting' or EventInfo[ii] == 'sit': # for ACEM
					dummyChkVal=[]
					for j in range(idx1,idx2+1):
						if validPtTPA[j] ==1:
							dummyChkVal.append(TPA_all[j])
					# pdb.set_trace()
					if not dummyChkVal:#check if any Data exists or else set to 0
						meanTPA_rest = 0
					else:
						meanTPA_rest = np.mean(dummyChkVal)
					# pdb.set_trace()

				if EventInfo[ii] == 'arm up' or EventInfo[ii] =='hand up': # for ACEM
					dummyChkVal=[]
					for j in range(idx1,idx2+1):
						if validPtTPA[j] ==1:
							dummyChkVal.append(TPA_all[j])
					# pdb.set_trace()
					if not dummyChkVal:#check if any Data exists or else set to 0
						meanTPA_armUp=0
					else:
						meanTPA_armUp = np.mean(dummyChkVal)
					# pdb.set_trace()

				if EventInfo[ii] == 'arm down' or EventInfo[ii] =='hand down': # for ACEM
					dummyChkVal=[]
					for j in range(idx1,idx2+1):
						if validPtTPA[j] ==1:
							dummyChkVal.append(TPA_all[j])
					# pdb.set_trace()
					if not dummyChkVal: #check if any Data exists or else set to 0
						meanTPA_armDown=0
					else:
						meanTPA_armDown = np.mean(dummyChkVal)

				# print"End of loop - to go again"
				# pdb.set_trace()

				# mapStdResults.append(np.std(maPres[idx1:idx2+1]))
			# print"End of loop !!"
			# pdb.set_trace()
			AvgResultsTimeSegment['Pressure_MAP_(mmHg)_Ch{}'.format(i+1)] = np.asarray(mapMeanResults)
			StdResultsTimeSegment['Pressure_MAP_(mmHg)_Ch{}'.format(i+1)] = np.asarray(mapStdResults)
			AvgResultsTimeSegment['Pressure_Frq_(bpm)_Ch{}'.format(i+1)] = np.asarray(mapFrqMeanResults)
			StdResultsTimeSegment['Pressure_Frq_(bpm)_Ch{}'.format(i+1)] = np.asarray(mapFrqStdResults)

			allTPAdata =[] # for ACEM
			for x, y in zip(TPA_all, validPtTPA):
				if y ==1:
					allTPAdata.append(x)
			maxTPA_overall = np.max(np.asarray(allTPAdata))
			# pdb.set_trace()

	#Creating excel file for reporting
	workbook = xlsxwriter.Workbook(output_file[:-4] + ".xlsx")
	cell_format_gnl2dp = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1,
									   'num_format':'0.00'})
	cell_format_gnl2dpRd = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1,
									   'num_format':'0.00',
									   'bg_color':'#FFEDCC'})
	cell_format_gnl0dp = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1,
									   'num_format':'#0'})
	cell_format_gnl0dpRd = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1,
									   'num_format':'#0',
									   'bg_color':'#FFEDCC'})
	cell_format_gnl = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1})
	cell_format_gnl1 = workbook.add_format({'align': 'center',
									   'valign': 'vcenter',
									   'border': 1})
	cell_format_gnl1.set_pattern(1)
	cell_format_gnl1.set_bg_color('#FFCACA')
	cell_format_gnlLtAlgnBlb = workbook.add_format({'align': 'left',
									   'valign': 'vcenter',
									   'border': 1,
									   'bold':1})
	cell_format_bld = workbook.add_format({'align': 'center',
										   'valign': 'vcenter',
										   'border': 1,
										   'bold': 1})


	#Patient Infor into Excel
	worksheet = workbook.add_worksheet("PatientInfo")
	worksheet.set_tab_color('blue')
	worksheet.set_column('A:A',30)
	worksheet.set_column('B:B',50)
	worksheet.protect("abell")
	row = 0
	col = 0
	patient_text = '\n'.join(['{0}: {1}'.format(k,v) for k, v in sorted(patient_info.items())])

	# worksheet.write(row, col , patient_text)
	#PatientInfo
	for keyy,vallue in sorted(patient_info.items()):
		worksheet.write(row, col , keyy, cell_format_gnlLtAlgnBlb)
		worksheet.write(row, col+1 , vallue, cell_format_gnl)
		col += 0
		row += 1

		# #Event export
	worksheet3 = workbook.add_worksheet("Summary analysis(Mean)")
	worksheet3.protect("abell")
	worksheet3.set_tab_color('green')

	row = 1
	col = 1

	#Header
	worksheet3.write(row,col,"Event - Event", cell_format_bld)
	worksheet3.write(row,col+1,"Time segment (s)", cell_format_bld)
	worksheet3.write(row,col+2,"Time segment (min:s)", cell_format_bld)
	worksheet3.write(row,col+3,"Time segment (HR:MIN:S)", cell_format_bld)
	worksheet3.set_column(1,10,25)

	row+=1

	for i in range(0,len(EvtTime)-1):
		worksheet3.write(row+i,col,EventInfo[i] + " - "+EventInfo[i+1], cell_format_bld)
		worksheet3.write(row+i,col+1, "%d - %d" % (EvtTime[i], (EvtTime[i+1])), cell_format_gnl)
		worksheet3.write(row+i,col+2, "%d:%d - %d:%d" % (divmod(EvtTime[i],60)+divmod(EvtTime[i+1],60)), cell_format_gnl)
		worksheet3.write(row+i,col+3, "%s - %s" % (EventInfoTimeHMS[i], (EventInfoTimeHMS[i+1])), cell_format_gnl)
	col+=4


	for key, keyStd in zip(AvgResultsTimeSegment.keys(), StdResultsTimeSegment.keys()):
		#Stuff to do
		row = 1
		worksheet3.write(row, col, key, cell_format_bld)
		for item,itemStd in zip(AvgResultsTimeSegment[key], StdResultsTimeSegment[keyStd]):
			row += 1
			if np.isnan(item):
				worksheet3.write(row, col, "No Good Data", cell_format_gnl1)
			else:
				# pdb.set_trace()
				if (key[0:4]=='Pres') and (itemStd==0):
					cell_formatting=cell_format_gnl0dpRd
				elif (key[0:4]=='Pres') and (itemStd==1):
					cell_formatting=cell_format_gnl0dp
				elif (key[0:3] =='ECG') and (itemStd==0):
					cell_formatting=cell_format_gnl0dpRd
				elif (key[0:3] =='ECG') and (itemStd==1):
					cell_formatting=cell_format_gnl0dp
				elif (key[0:3] =='EGG') and (itemStd==0):
					cell_formatting=cell_format_gnl2dpRd
				elif (key[0:3] =='EGG') and (itemStd==1):
					cell_formatting=cell_format_gnl2dp

				# worksheet3.write(row, col , "%3.2f+%3.2f"%(item,itemStd), cell_formatting)
				worksheet3.write(row, col , item, cell_formatting)

		col += 1


	#Data Estimates into Excel
	worksheet1 = workbook.add_worksheet("DataEstimatesAll")
	worksheet1.set_tab_color('red')
	worksheet1.set_column(0,30,25)
	worksheet1.protect("abell")
	row = 0
	col = 0
	iterCol = 1


	for key in DictOverall.keys():
		row += 1
		worksheet1.write(row, col, key, cell_format_bld)
		for item in DictOverall[key]:
			# print item
			row += 1
			worksheet1.write(row, col , item, cell_format_gnl)
		col+=iterCol
		row = 0

	#Event export into excel
	# worksheet2 = workbook.add_worksheet("Event markers")
	# worksheet2.protect("abell")
	# worksheet2.set_column(0,30,20)
	# worksheet2.set_tab_color('gray')
	# row = 1
	# col = 0
	#
	# for eventType in events:
	# 	print eventType
	# 	if eventType[0]!= 'Time':
	# 		# pdb.set_trace()
	# 		worksheet2.write(row,col,eventType[0], cell_format_gnlLtAlgnBlb)	#EVt No
	# 		row+=1
	# 		worksheet2.write(row,col,eventType[1][0], cell_format_gnlLtAlgnBlb)	#EVT TP
	# 		worksheet2.write(row,col+1,eventType[1][1], cell_format_gnl)	#EVT TP info
	# 		row+=1
	# 		worksheet2.write(row,col,eventType[5][0], cell_format_gnlLtAlgnBlb)	#Time TP
	# 		worksheet2.write(row,col+1,eventType[5][1], cell_format_gnl)	#Time info
	# 		row+=2
	# 	elif eventType[0]=='Time':
	# 		row+=3
	# 		worksheet2.write(row,col,'Start Time', cell_format_gnlLtAlgnBlb)
	# 		row+=1
	# 		worksheet2.write(row,col,eventType[0], cell_format_gnlLtAlgnBlb)	#Time info
	# 		worksheet2.write(row,col+1,eventType[1], cell_format_gnl)	#Time info



	#ACEM export into excel
	worksheet2 = workbook.add_worksheet("ACEM Metrics")
	worksheet2.protect("abell")
	worksheet2.set_column(0,30,20)
	worksheet2.set_tab_color('gray')
	# row = 1
	# col = 1
	worksheet2.set_column('B:C', 35)

	# variables for ACEM analysis - to rearrange accordingly - need to get EGG as well
	worksheet2.merge_range('B2:C2', 'Autonomic nervous system evaluation (ACEM)', cell_format_bld)
	worksheet2.merge_range('B4:C4', 'Parasympathetic Nerve Function',cell_format_bld)
	worksheet2.write('B5', 'Maximum HR (bpm)',cell_format_gnl)
	try:
		worksheet2.write('C5', maxHeartRate,cell_format_gnl0dp)
	except:
		print "no min heart rate"
		worksheet2.write('C5', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B6', 'Minimum HR (bpm)',cell_format_gnl)
	try:
		worksheet2.write('C6', minHeartRate,cell_format_gnl0dp)
	except:
		print "no max heart rate"
		worksheet2.write('C6', 'NA - data not present' ,cell_format_gnl)


	worksheet2.write('B7', 'Change in HR with breathing (%)',cell_format_gnl)
	try:
		percentChangeHeartRate =((maxHeartRate -changeHR_breating) /  minHeartRate) *100.00 # correct way
		# percentChangeHeartRate =((maxHeartRate -minHeartRate) /  minHeartRate) *100.00 #but this is how the numbers are calculated on acem report
		worksheet2.write('C7', percentChangeHeartRate, cell_format_gnl2dp)
	except:
		print "no metrics for computation of change in hr breating"
		worksheet2.write('C7', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B8', 'Valsalva ratio',cell_format_gnl)
	try:
		valsalva = (maxHeartRate /minHeartRate)
		worksheet2.write('C8', valsalva, cell_format_gnl2dp)
	except:
		print "no metrics for valsalva ratio"
		worksheet2.write('C8', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B9', '30/15 ratio',cell_format_gnl)

	try:
		thirty_fifteenRatio=(HR_sitting_last / HR_standing_first)
		worksheet2.write('C9', thirty_fifteenRatio, cell_format_gnl2dp)
	except:
		print "no metrics for 30/15 ratio"
		worksheet2.write('C9', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B10', 'Cardiovascular function (CVF) score',cell_format_bld)
	try:
		worksheet2.write('C10', percentChangeHeartRate+ valsalva+thirty_fifteenRatio, cell_format_gnl2dp)
	except:
		print "no metrics for cvf"
		worksheet2.write('C10', 'NA - data not present' ,cell_format_gnl)



	worksheet2.merge_range('B12:C12', 'Sympathetic Adrenergic Functions',cell_format_bld)
	worksheet2.write('B13', 'Mean TPA Rest',cell_format_gnl)
	try:
		worksheet2.write('C13',meanTPA_rest, cell_format_gnl2dp)
	except:
		print "no metrics for mean TPA at rest"
		worksheet2.write('C13', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B14', 'Mean TPA Arm down',cell_format_gnl) ##
	try:
		worksheet2.write('C14',meanTPA_armDown, cell_format_gnl2dp)
	except:
		print "no metrics for mean TPA arm down"
		worksheet2.write('C14', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B15', 'Mean TPA Arm up',cell_format_gnl) ##
	try:
		worksheet2.write('C15',meanTPA_armUp, cell_format_gnl2dp)
	except:
		print "no metrics for mean TPA arm up"
		worksheet2.write('C15', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B16', 'Max TPA Stand',cell_format_gnl)
	try:
		worksheet2.write('C16',maxTPA_stand, cell_format_gnl2dp)
	except:
		print "no metrics for max TPA at stand"
		worksheet2.write('C16', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B17', 'Min TPA Stand',cell_format_gnl)
	try:
		worksheet2.write('C17',minTPA_stand, cell_format_gnl2dp)
	except:
		print "no metrics for min TPA at stand"
		worksheet2.write('C17', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B18', 'Postural Adjustment Ratio',cell_format_gnl)
	try:
		pra_TAP = maxTPA_overall/minTPA_stand
		worksheet2.write('C18', pra_TAP, cell_format_gnl2dp)
	except:
		print "no metrics for PAR"
		worksheet2.write('C18', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B19', 'Mean TPA Cold Stimulus',cell_format_gnl)
	try:
		worksheet2.write('C19',meanTPA_coldStim, cell_format_gnl2dp)
	except:
		print "no metrics for TPA cold"
		worksheet2.write('C19', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B20', 'Vasoconstriction (%)',cell_format_gnl)
	try:
		vasoTAP = (1 - (meanTPA_coldStim/meanTPA_rest))*100.00
		worksheet2.write('C20',vasoTAP, cell_format_gnl2dp)
	except:
		print "no metrics for % vaso tpa"
		worksheet2.write('C20', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B21', 'Sympathetic Adrenergic Function (SAF) score',cell_format_bld)
	try:
		worksheet2.write('C21', vasoTAP + pra_TAP, cell_format_gnl2dp)
	except:
		print "no metrics for saf score"
		worksheet2.write('C21', 'NA - data not present' ,cell_format_gnl)


	worksheet2.merge_range('B23:C23', 'Electrogastrography (EGG)',cell_format_bld)
	worksheet2.write('B24', 'EGG resting (cpm)',cell_format_gnl)
	try:
		worksheet2.write('C24', np.nanmean(np.asarray(egg_restingState)), cell_format_gnl2dp)
	except:
		print "no metrics for EGG  resting score"
		worksheet2.write('C24', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B25', 'EGG standing (cpm)',cell_format_gnl)
	try:
		worksheet2.write('C25', np.nanmean(np.asarray(egg_standingState)), cell_format_gnl2dp)
	except:
		print "no metrics for EGG standing"
		worksheet2.write('C25', 'NA - data not present' ,cell_format_gnl)

	worksheet2.write('B26', 'EGG Cold Stress (cpm)',cell_format_gnl)
	# pdb.set_trace()
	try:
		worksheet2.write('C26', np.nanmean(np.asarray(egg_ColdStimState)), cell_format_gnl2dp)
	except:
		print "no metrics for EGG cold stress score"
		worksheet2.write('C26', 'NA - data not present' ,cell_format_gnl)



	# sympathetic
	# changeHR_breating
	# HR_standing_first
	# HR_sitting_last
	# maxHeartRate
	# minHeartRate
	#
	#parasympathetic
	# maxTPA_stand
	# minTPA_stand
	# meanTPA_coldStim
	# meanTPA_rest
	# maxTPA_overall
	#
	#egg
	# egg_ColdStimState
	# egg_standingState
	# egg_restingState
	#
	# pdb.set_trace()


	workbook.close()
	# pdb.set_trace()


	box4 = wx.MessageDialog(None, "Data analysis completed - Excel sheet generated", "Cutaneous EGG analysis", wx.OK)
	if box4.ShowModal()==wx.ID_OK:
		box4.Destroy()

if __name__ == '__main__':

	#Testing writing to excel worksheet
	workbook = xlsxwriter.Workbook('data.xlsx')
	worksheet = workbook.add_worksheet()

	da = {'a':['e1','e2','e3'],'b':['e1','e2'],'c':['e1']}
	row = 0
	col = 0
	iterC = 3

	for key in da.keys():
		row += 1
		worksheet.write(row, col,     key)
		for item in da[key]:
			worksheet.write(row, col + 1, item)
			row += 1
		col +=iterC
		row = 0

	workbook.close()
