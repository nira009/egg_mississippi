# -*- coding: iso-8859-1 -*-

import numpy as np
#import scipy as sci
from scipy import signal as scipySig
from scipy import fft
from matplotlib import pyplot as plt
import matplotlib.pyplot as plt1
import pdb as pdb
import time as time
import peakfind as peakfind
import numpy as np
import findIndices as findIndices
from scipy.signal import savgol_filter
from scipy.signal import hilbert # not used for not - testing only - remove when compiling
from scipy.signal import savgol_filter
from scipy.interpolate import interp1d
import peakutils # not used for now - testing only - remove when compiling
from scipy import sparse
from scipy.sparse.linalg import spsolve
from copy import deepcopy

class SignalError(Exception):
	def __init__(self, value):
		self.value = value
	def __str__(self):
		return repr(self.value)

def baseline_als(y, lam, p, niter=10):
  L = len(y)
  # D = sparse.csc_matrix(np.diff(np.eye(L), 2))
  diag = np.ones(L - 2)
  D = sparse.spdiags([diag, -2*diag, diag], [0, -1, -2], L, L-2)
  w = np.ones(L)
  for i in xrange(niter):
    W = sparse.spdiags(w, 0, L, L)
    Z = W + lam * D.dot(D.transpose())
    z = spsolve(Z, w*y)
    w = p * (y > z) + (1-p) * (y < z)
  return z

class Signal(object):
	def __init__(self, sig=None, t=None, fs=None):
				
		if sig is None:
			sig = 60 * np.sin(np.linspace(0, 6 * np.pi, num=10000))
		
		self.sig = sig
		self.rawSig = sig
		
		if t is not None:
			self.t = t
			self.fs = 1. / (self.t[2]-self.t[1])
		elif fs is not None:
			self.fs = float(fs)
			self.t = np.arange(len(self.sig)) * (1 / self.fs)
		else:
			raise SignalError('Require either fs or t vector, have neither')
	def __repr__(self):
		return self.sig.__repr__()
	#def __repr__(self):
	#	return 'Signal ' + self.sig.__repr()
	def __str__(self):
		return 'Signal of length {0}'.format(len(self.sig))


def dummy_filter(signal):
	'''
	dummy_filter(signal) 
	takes a signal, returns a new signal (a copy of the first)
	all filters should follow this format
	'''
	return Signal(np.copy(signal.sig), t=np.copy(signal.t))

def create_baseline_filter(median_window=20):
	'''
	Baseline estimation using a moving median filter. The parameters for the 
	filters are set at median_window=20. The output is an object with the
	signal where the baseline is removed. 
	'''
	print "creating baseline filter now"
	def baseline_filter(signal):
		if np.mod(median_window * signal.fs, 2) == 0: #make window odd
			win_med = (median_window * signal.fs) + 1
		else:
			win_med = median_window * signal.fs
		print "median filter estimation"
		G = scipySig.medfilt(signal.sig, win_med)
		print "subtracting baseline wander"
		
		#Applying a low pass filter to smooth out the baseline wander
		filtOrder= 2
		nyq = signal.fs / 2 
		cutoff= 0.025
		[b, a] = scipySig.butter(filtOrder, cutoff / float(nyq), btype='low', analog=0)
		H = scipySig.filtfilt(b, a, G) 
		
		#pdb.set_trace()
		output_signal = signal.sig - G

		return Signal(output_signal, np.copy(signal.t))

	return baseline_filter


def lowpass_filter(filtOrder=  5,cutoff = 1.5):
	'''
	Low pass butterworth filtering. The parameters for the filters are set
	at filtOrder=  5,cutoff = 1.5 Hz. The output is an object with the
	signal filtered.
	'''
	def butterworth_lowpass_filtering(signal):
		nyq = signal.fs / 2
		print "Butterworth filter"
		[b, a] = scipySig.butter(filtOrder, cutoff / float(nyq), btype='low', analog=0)
		# output_signal = scipySig.filtfilt(b, a, signal.sig)

		output_signal=savgol_filter(signal.sig, 191, 3) # set as 151 window size znd poly order as 3

		return Signal(output_signal, t=np.copy(signal.t))

	return butterworth_lowpass_filtering

def lowpass_ButterFilter(filtOrder=  5,cutoff = 1.5):
	'''
	Low pass butterworth filtering. The parameters for the filters are set
	at filtOrder=  5,cutoff = 1.5 Hz. The output is an object with the
	signal filtered.
	'''
	def butterworth_lowpass_filtering(signal):
		nyq = signal.fs / 2
		print "Butterworth filter"
		[b, a] = scipySig.butter(filtOrder, cutoff / float(nyq), btype='low', analog=0)
		output_signal = scipySig.filtfilt(b, a, signal.sig)

		# output_signal=savgol_filter(signal.sig, 191, 3) # set as 151 window size znd poly order as 3

		return Signal(output_signal, t=np.copy(signal.t))

	return butterworth_lowpass_filtering

def highpass_filter(filtOrder=  2,cutoff = .01):
	'''
	High pass butterworth filtering. The parameters for the filters are set
	at filtOrder=  2,cutoff = .01 Hz. The output is an object with the
	signal filtered.
	'''
	def butterworth_highpass_filtering(signal):
		nyq = signal.fs / 2
		print "Butterworth filter"
		[b, a] = scipySig.butter(filtOrder, cutoff / float(nyq), btype='high', analog=0)
		output_signal = scipySig.filtfilt(b, a, signal.sig) 
		
		return Signal(output_signal, t=np.copy(signal.t))

	return butterworth_highpass_filtering

def highpass_als_filter(lam0=1000000, p0 = 0.005, lam1=1000, p1=0.05):
	'''
	Baseline filtering using als to estimate baseline and subtract signal
	'''
	def baseline_als_filtering(signal):
		nyq = signal.fs/2
		# lam0 =1000000
		# p1 = 0.005
		baselineLSS = baseline_als(signal.sig, lam0, p0, niter=10)
		# lam1 =1000
		# p1 = 0.05
		baselineLSS = baseline_als(baselineLSS, lam1, p1, niter=10)
		# plt.figure()
		# plt.plot(signal.t,signal.sig)
		# plt.plot(signal.t,baselineLSS)
		# plt.plot(signal.t,signal.sig-baselineLSS)
		# plt.show()
		output_signal = signal.sig-baselineLSS
		output_signal = output_signal  - np.median(output_signal)
		return Signal(output_signal, t=np.copy(signal.t))
	return baseline_als_filtering

def nan_helper(y):
    """Helper to handle indices and logical indices of NaNs.

    Input:
        - y, 1d numpy array with possible NaNs
    Output:
        - nans, logical indices of NaNs
        - index, a function, with signature indices= index(logical_indices),
          to convert logical indices of NaNs to 'equivalent' indices
    Example:
        >>> # linear interpolation of NaNs
        >>> nans, x= nan_helper(y)
        >>> y[nans]= np.interp(x(nans), x(~nans), y[~nans])
    """

    return np.isnan(y), lambda z: z.nonzero()[0]

def amplitude_frequency_estimation(window = 3600, data_Overlap = 1800):
	'''
	This function is responsible for estimating the frequency and amplitude 
	of the signal in a running manner. The input parameters for the running 
	window are window = 3600, data_Overlap = 1800.
	The output is the  max frequency, max and min overlap and amplitude.
	The order of the output is as follows maxFrqs,MaxLimit,MinLimit,Ampl
	'''
	#Computing the rolling overlaps of the signals 
	def running_ampl_freq_est(signal):
		# plt.figure(),plt.plot(signal.t,signal.sig),plt.show()

		signal1 = deepcopy(signal)

		sig_rollin = rolling_window_overlap(signal1.sig,window,data_Overlap)
		t_rollin = rolling_window_overlap(signal1.t,window,data_Overlap)
		t_rollin = np.median(t_rollin,axis=1)

		#Amplitude estimation 
		#MaxLimit = np.max(sig_rollin,-1)
		#MinLimit = np.min(sig_rollin,-1)
		#Ampl = MaxLimit - MinLimit
		#pdb.set_trace()
		
		Ampl = [0] * sig_rollin.shape[0]
		MaxLimit = [0] * sig_rollin.shape[0]
		MinLimit = [0] * sig_rollin.shape[0]
		
		for i in range(0,sig_rollin.shape[0]) :
#			sigAmpl_rollin = rolling_window_overlap(sig_rollin[i],window = 600, data_Overlap = 60)
			sigAmpl_rollin = rolling_window_overlap(sig_rollin[i],window = 600, data_Overlap = 500) #changed as of 24March2014
			subMaxAmpl = np.max(sigAmpl_rollin,-1)
			subMinAmpl = np.min(sigAmpl_rollin,-1)
			subAmpl = subMaxAmpl - subMinAmpl
			#print "Median %f" % np.median(Ampl)
			#print "Mean %f" % np.mean(Ampl)
			#print "Std %f" % np.std(Ampl)
			MaxLimit[i] = np.median(subMaxAmpl)
			MinLimit[i] = np.median(subMinAmpl)
			Ampl[i] = np.median(subAmpl)

				
		##Frequency estimation 
#		fourier = np.fft.fft(sig_rollin)
#		freqs = np.fft.fftfreq(sig_rollin.shape[1], d=1/signal.fs)
#	
#		fourier = np.abs(fourier[0:len(fourier),freqs>=0])
#		freqs = freqs[freqs>=0]
		




		# pdb.set_trace()
		cutoff=float(30/60)
		filtOrder=3
		[b, a] = scipySig.butter(filtOrder, cutoff / float(15), btype='high', analog=0)
		for i in range(0,sig_rollin.shape[0]):
			med = np.median(sig_rollin[i])
			cutOff = np.median(np.abs(sig_rollin[i]-med))
			cutOff1 = np.mean(sig_rollin[i])+ 1.5*np.std(sig_rollin[i], ddof = 1)
			A = np.where(sig_rollin[i]>cutOff1)
			cutOff1A = np.mean(sig_rollin[i])- 1.5*np.std(sig_rollin[i], ddof = 1)
			B = np.where(sig_rollin[i]<cutOff1A)

			sig_rollin[i,A]=np.nan
			sig_rollin[i,A]=np.nan

			nans, x= nan_helper(sig_rollin[i])
			sig_rollin[i,nans]= np.interp(x(nans), x(~nans), sig_rollin[i,~nans])
			# sig_rollin[i]=savgol_filter(sig_rollin[i], 61, 3)
			sig_rollin[i] = scipySig.filtfilt(b, a, sig_rollin[i])

		# plt.figure(),plt.plot(signal.t,signal.sig),plt.show()

		# pdb.set_trace()
		#Frequency estimation - NP - 27/Dec/2012
		#0. Detrend signals
		sig_rollin = scipySig.detrend(sig_rollin, axis=1, type='constant')


		# 1. Window signals
		hannWind = scipySig.blackman(sig_rollin.shape[1], sym=True) # was hann
		hannWind = np.tile(hannWind, [sig_rollin.shape[0], 1])

		sig_rollin = sig_rollin*hannWind

		#2. Zero padding for the fft signal
		L = nextpow2(sig_rollin.shape[1])
		fourier = fft(sig_rollin,int(L))
		fourier = np.abs(fourier[0:len(fourier)**2,0:L/2])**2
		freqs = np.linspace(0, 15, L/2)

		# pdb.set_trace()

		maxFrqs = freqs[fourier.argmax(axis=1)]
		print "Frequency (cpm) for entire time - " + str(maxFrqs*60) #Convert to cpm
		print "Amplitude (mV) for entire time - " + str(Ampl)

		# Finding no of peaks in the frequency domain of 0 to 10 cpm - ideally one component
		FrqPeaks=[0] * fourier.shape[0]
		indxFrqTenCpm =   findIndices.indices(freqs, lambda x: x <= 12/60.0)[-1]	#chosen 12 for rounding issues
		for i in range(0,fourier.shape[0]):
			
			#t = time.time()
			#[_max, _min] = peakfind.peakdetect(fourier[i,0:indxFrqTenCpm]/float(np.max(fourier[i,:])),
			#								 x_axis = freqs[0:indxFrqTenCpm]*60.0, lookahead = 150, delta=0)

			[_max, _min] = peakfind.peakdetect(fourier[i,0:indxFrqTenCpm]/float(np.max(fourier[i,:])),
											 x_axis = freqs[0:indxFrqTenCpm]*60.0, lookahead = 25, delta=0)
						
			#elapsed = time.time() - t
			#print 'time spent peak find',elapsed
			
			#t1 = time.time()
			xm = [p[0] for p in _max]
			xmVal = [pVal[1] for pVal in _max]
			ym = [p[1] for p in _max]
			#elapsed = time.time() - t1
			#print elapsed
			
			#t2 = time.time()
			inds = findIndices.indices(xmVal, lambda x: x > 0.75)
			#inds =  findIndices.indices(xm, lambda x: x < 10)
			#elapsed = time.time() - t2
			#print elapsed
			
			FrqPeaks[i]= len(inds)

			#Debugging code
			debugLoop = 0
			if debugLoop:
				print i
				print len(inds)
				print FrqPeaks[i]
			
			debugPlt = 1
##			if debugPlt:
#			if  i==158 or i ==159 or i ==160 or i ==215 or i ==216 or i ==217:
#				plt.figure
#				plt.ax1=plt.subplot(211)
#				plt.plot(sig_rollin[i,:])
#				plt.title(i)
#				plt.ax1=plt.subplot(212)
#				plt.plot(freqs*60,fourier[i,:])
#				plt.hold(True)
#				plt.plot(xm, ym, 'r+')
#				pdb.set_trace()
#				plt.show()
#				#xn = [p[0] for p in _min]
#				#yn = [p[1] for p in _min]
#				#plt.plot(freqs*60, fourier[i,:])
#				#plt.hold(True)
#				#plt.plot(xm, ym, 'r+')
#				#plt.plot(xn, yn, 'g+')

#		pdb.set_trace()
		return  maxFrqs,MaxLimit,MinLimit,Ampl,t_rollin,fourier,freqs,FrqPeaks

	return running_ampl_freq_est


#Helper function to compute the next power of two
def nextpow2(n):
	m_f = np.log2(n)
	m_i = np.ceil(m_f)
	return (2**m_i)*20

#Computing the rolling overlaps of the signals 
def rolling_window_overlap(a, window, data_Overlap):
	'''
	This method seperates the signals into stacks or strides of the signal
	for computation of rolling stats. By default this method is called from
	the method 'amplitude_frequency_estimation' in this class.
	'''

	data_noOverlap = window - data_Overlap
	indices = np.arange(start=0, stop=a.size - window + 1, step=data_noOverlap)
	shape = indices.shape[-1], window
	strides = (a.strides[-1] * data_noOverlap,) + a.strides 
	return np.lib.stride_tricks.as_strided(a, shape=shape, strides=strides)

def mask_bad_sections(signal, sig_min=None, sig_max=None, expand_points=0):
	'''
	Takes a Signal object as input, and optional sig_min and sig_max
	values. 
	Returns a new Signal object where any values below sig_min or above
	sig_max are masked, as a numpy.ma masked array.
	One of sig_min and sig_max may be ommitted.  The optional argument,
	expand_points, is used to expand the extent of masked regions, i.e.
	if sig[i] is masked, for any sig[j] where |i-j| <= expand_points
	sig[j] is masked.
	'''
	if sig_min is None and sig_max is None:
		# asked to do nothing
		return signal
	output_signal = Signal(signal.sig.copy(), signal.t)
	if sig_min is None:
		output_signal.sig = np.ma.masked_greater(output_signal.sig, sig_max)
	elif sig_max is None:
		output_signal.sig = np.ma.masked_less(output_signal.sig, sig_min)
	else:
		output_signal.sig = np.ma.masked_outside(output_signal.sig, sig_min, sig_max)
		
	# now will we grow the mask?
	mask = output_signal.sig.mask
	new_mask = mask.copy()
	for shift in range(1, expand_points+1):
		shifted_mask = np.concatenate((np.zeros(shift, np.bool), mask[:-shift]))
		new_mask |= shifted_mask
		shifted_mask = np.concatenate((mask[shift:], np.zeros(shift, np.bool)))
		new_mask |= shifted_mask
	
	output_signal.sig = np.ma.masked_array(output_signal.sig, new_mask)
	return output_signal

def mask_ampl_frq(ampl,freq, ampl_min = None, ampl_max = None, freq_min = None, freq_max = None):
	'''
	This function masks the amplitude and frequency vector according to 
	specified thresholds. 
	Required inputs - amplitude vector and - frequency vector
	Optional inputs - min and max thresholds for amplitude and frequency
	Output - masked/unmasked amplitude and frequency vector  
	'''
	if ampl_min is None and ampl_max is None and freq_min is None and freq_max is None:
		#asked to do nothing
		return ampl,freq
	
	# Amplitude masking
	if ampl_min is None and ampl_max is not None:
		ampl = np.ma.masked_greater(ampl,ampl_max)
	elif ampl_max is None and ampl_min is not None:
		ampl = np.ma.masked_less(ampl,ampl_min)
	elif ampl_max is not None and ampl_min is not None:
		ampl = np.ma.masked_outside(ampl,ampl_min,ampl_max)
	
	# Frequency masking
	if freq_min is None and freq_max is not None:
		freq = np.ma.masked_greater(freq,freq_max)
	elif freq_max is None and freq_min is not None:
		freq = np.ma.masked_less(freq,freq_min)
	elif freq_max is not None and freq_min is not None:
		freq = np.ma.masked_outside(freq,freq_min,freq_max)
	 
	return ampl,freq

		

if __name__ == '__main__':
	
	print "Creating dummy signal for test - var created is 'sig'"

	#Initialise var
	fs = 30.0
	T = 1 / fs
	L = 20000
	t = np.linspace(0,L-1,L)
	t = t/fs

	#Creating synthetic signal with two freq components with baseline wander
	
	# sine wave representing EGG at 3 cpm
	s = 60 * np.sin(2*np.pi*0.05*t) 
	#sine wave representing EGG
	s[10000:20000] = 30 * np.sin(2*np.pi*t[10000:20000]*0.1)
	s = s + 90 * np.sin(2*np.pi*.0004*t) # baseline wander
	
	# High frequency artifact eg cough event
	s[15000:15060]=300 * np.sin(2*np.pi*1*t[15000:15060]) 
	
	
	
	#Initialise test object
	Sig1 = Signal(sig = s,fs = fs)
	
	#Baseline removal
	bas_filt = create_baseline_filter(median_window=20)
	Sig1 = bas_filt(Sig1)
	
	#High frequency removal
	lp_filt = lowpass_filter(filtOrder=  5,cutoff = 1.5) # WAS 5 AND 1.5
	Sig1 = lp_filt(Sig1)
	
	#Amplitude and frequency estimation and the FAR
	sig_rollin = rolling_window_overlap(Sig1.sig,window=1800,data_Overlap=900)
	ampl_frq_calc = amplitude_frequency_estimation(window = 1800, data_Overlap = 900)
	[maxfr,maxy,mini,ampl,t,fourier,freqs] = ampl_frq_calc(Sig1)

	#Masking signal
	Sig1 = mask_bad_sections(signal=Sig1, sig_min=-100, sig_max=100, expand_points=10) 
	[ampl,maxfr] = mask_ampl_frq(ampl,maxfr, ampl_min = -100, ampl_max = 100, freq_min = 2, freq_max = 20)
	
	FAR = maxfr / ampl


	


