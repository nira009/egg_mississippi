# -*- coding: iso-8859-1 -*-

import numpy as np
import scipy as sci
from scipy import signal
import matplotlib.pyplot as plt
from numpy.random import normal
from scipy.signal.waveforms import sawtooth
import copy
import pdb as pdb

import signal_egg as sigEGG

def find_nearest_indx(array, value):
    '''
    Helper function to find the nearest value in an array
    '''
    idx = (np.abs(array - value)).argmin()
    return idx


def calcFrqPercentage1(fftFrq, fft, indx_frq01, indx_frq10):
    '''
    Estimate signal integrity measure. Here the % power of the fft signals are 
    given in the range of specified intervals
    '''
    Meas = [None] * len(fft)

    for i in np.arange(0, len(fft)):
#        fft[i]=(fft[i]/np.max(fft[i]))*100
        Meas[i] = (np.sum(fft[i][indx_frq01:indx_frq10]) / np.sum(fft[i])) * 100
#        pdb.set_trace()
    print "almnise done"
    return Meas
    
    
def calcFrq(fftFrq,fft):
  #Make copy of variables 
  fftCp = copy.deepcopy(fft)
  MaxFrqInx = fft.argmax(axis=1)  
  Meas = [None]*len(fft)
  # Loop throough to find the measures
  #pdb.set_trace()
#  plt.figure()

  for i in np.arange(0,len(fft)):
    #plt.figure()
    fft[i]=fft[i]/sum(fft[i])
    fftCp[i]=fftCp[i]/sum(fftCp[i])
    
    vSTART=MaxFrqInx[i]-2
    vSTOP=MaxFrqInx[i]+2
    #plt.plot(fft[i])
    
    if vSTART < 0:
      vSTART=0
      
    if vSTOP >len(fft):
      vSTOP = len(fft)
    
    print(vSTART,vSTOP)
      
    fftCp[i][vSTART:vSTOP]=0
    #plt.plot(fftCp[i])
    #pdb.set_trace()
    OtherMax = max(fftCp[i])
    
    Meas[i]=max(fft[i])/OtherMax
    Meas[i]=max(fft[i])/sum(fft[i])
    Meas[i]=OtherMax
    
    print(max(fft[i]),sum(fftCp[i]))
    print(Meas[i])
    #pdb.set_trace()

  G = 100
  return Meas

    
    
print "Creating dummy signal for test - var created is 'sig'"

#Initialise var
fs = 30.0
T = 1 / fs
L = 20000
t = np.linspace(0,L-1,L)
t = t/fs

##Creating synthetic signal with two freq components with baseline wander
#
## sine wave representing EGG at 3 cpm
##s[10000:20000] = 30 * np.sin(2*np.pi*t[10000:20000]*0.1)
##b = 90 * np.sin(2*np.pi*.0004*t) + 20*np.cos(2*np.pi*.004*t)  # baseline wander
#s = 60 * np.sin(2*np.pi*0.05*t) 
#b = 200 * np.sin(2*np.pi*.0004*t) - 150*np.cos(2*np.pi*.0004*t-20)*-1 # baseline wander
#
#A = 50*signal.sin(2 * np.pi * 0.05 * t )
#B = 50*signal.sin(2 * np.pi * 0.05 * t )
#
##Artificial random normal noise
#npts=len(b)
#noise=20*normal(0,1,npts)  #mean, std dev, num pts
#noiseALL=80*normal(0,1,npts)  #mean, std dev, num pts
#
#
#s = A+noise
#s[10000:12000] = (s[10000:12000] / 4 ) + noiseALL[10000:12000]
#
##s[8000:20000] = B[8000:20000] + noise[8000:20000]
#
#
#
#plt.figure()
#plt.plot(s)
#plt.title('RawSig')
slow_wave=3.0/60.0
s = 60 * np.sin(2*np.pi*slow_wave*t)  + 10*normal(0,5,len(t))

#Initialise test object
Sig1 = sigEGG.Signal(sig = s,fs = fs)

#Baseline removal
bas_filt = sigEGG.create_baseline_filter(median_window=20)
Sig1 = bas_filt(Sig1)

#High frequency removal
lp_filt = sigEGG.lowpass_filter(filtOrder=  5,cutoff = 1.5)
Sig1 = lp_filt(Sig1)

#Amplitude and frequency estimation and the FAR
sig_rollin = sigEGG.rolling_window_overlap(Sig1.sig,window=1800,data_Overlap=1800-300)
ampl_frq_calc = sigEGG.amplitude_frequency_estimation(window = 1800, data_Overlap = 1800-300)
[maxfr,maxy,mini,ampl,t,fft,fftFrq] = ampl_frq_calc(Sig1)

plt.figure()
plt.plot(Sig1.t,Sig1.sig)
plt.title('Filtered signal')


plt.figure()
plt.plot(maxfr*60)
plt.title('Maximum Freq over time')

print maxfr*60

#
##Estimate some sort of signal quality measure
#indx_frq01 = int(find_nearest_indx(fftFrq, 1/60.0))
#indx_frq10 = int(find_nearest_indx(fftFrq, 10/60.0))
#plt.figure()
#plt.plot(fftFrq*60,fft[5,:])
#plt.show()
#pdb.set_trace()
#Meas = calcFrqPercentage1(fftFrq, fft, indx_frq01, indx_frq10)
#print "Signal intergre" +str(Meas)
#print "Done"

##Estimate some sort of signal quality measure
#Meas=calcFrq(fftFrq,fft)
#
#plt.figure()
#plt.plot(t,Meas,'*-')  
#plt.title('Signal integrity mesure ??')

plt.show()






